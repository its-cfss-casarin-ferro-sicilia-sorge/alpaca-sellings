import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellingsChartsComponent } from './components/sellings-charts/sellings-charts.component';
import { UserLoggedInGuard } from './guards/user-logged-in.guard';
import { UserNotLoggedInGuard } from './guards/user-not-logged-in.guard';
import { AcquistoComponent } from './pages/acquisto/acquisto.component';
import { FornitoreComponent } from './pages/fornitore/fornitore.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { MagazzinoComponent } from './pages/magazzino/magazzino.component';
import { VenditaComponent } from './pages/vendita/vendita.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'warehouse',
    component: MagazzinoComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'supplier',
    component: FornitoreComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'purchase',
    component: AcquistoComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'sellings',
    component: VenditaComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [UserNotLoggedInGuard]
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: 'sellings',
    component: SellingsChartsComponent,
    canActivate: [UserLoggedInGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
