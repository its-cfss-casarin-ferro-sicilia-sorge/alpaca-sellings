import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import ITable from 'src/app/interfaces/ITable';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { faExchangeAlt, faLongArrowAltDown, faLongArrowAltUp, faEdit, faTrashAlt, faAmbulance } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html'
})
export class TableComponent implements OnInit {
  faExchangeAlt = faExchangeAlt;
  faLongArrowAltDown = faLongArrowAltDown;
  faLongArrowAltUp = faLongArrowAltUp;
  faEdit = faEdit;
  faTrash = faTrashAlt;
  faAmbulance = faAmbulance;

  @Input()
  public table?: ITable;

  @Input()
  public showItemDetail?: boolean;

  @Input()
  public columns: ITableColumn[] = [];

  @Input()
  public rows: ITableRow[] = [];

  @Input()
  public pagesNumber = 0;

  @Input()
  public modifica = true;

  @Input()
  public caricoMagazzinoButton = false;

  @Output()
  onInputChange: EventEmitter<{ value: string, name: string; }> = new EventEmitter();

  @Output()
  onChangePage: EventEmitter<number> = new EventEmitter();

  @Output()
  onClearFilter: EventEmitter<string> = new EventEmitter();

  @Output()
  onDownFilter: EventEmitter<void> = new EventEmitter();

  @Output()
  onUpFilter: EventEmitter<string> = new EventEmitter();

  @Output()
  onSelectRow: EventEmitter<ITableRow> = new EventEmitter();

  @Output()
  onEditRow: EventEmitter<ITableRow> = new EventEmitter();

  @Output()
  onDeleteRow: EventEmitter<ITableRow> = new EventEmitter();

  @Output()
  onCaricoRow: EventEmitter<ITableRow> = new EventEmitter();

  public pagine: { number: number, active: boolean; }[] = [];
  public entrambi = true;
  public down = false;
  public up = false;
  public showTable = true;

  ngOnInit(): void {
    for (let i = 1; i <= this.pagesNumber; i++) {
      this.pagine.push({ number: i, active: false });
    }
    if (this.pagine[0]) {
      this.pagine[0].active = true;
    }
  }

  search(i: number, colonna: string) {
    if (this.columns[i].searchableValue !== '') {
      this.onInputChange.emit({ value: this.columns[i].searchableValue, name: colonna });
    }
  }

  showPreviousDots(): boolean {
    if (this.pagine[0] && !this.pagine[0].active && this.pagine.length > 3) {
      return true;
    }
    return false;
  }

  showNextDots(): boolean {
    if (this.pagine[this.pagine.length - 1] && !this.pagine[this.pagine.length - 1].active && !this.pagine[this.pagine.length - 2]?.active && !this.pagine[this.pagine.length - 3]?.active) {
      return true;
    }
    return false;
  }

  viewNummber(i: number): boolean {
    const index = this.pagine.findIndex(item => item.active);
    if ((index === this.pagine.length - 1 || index === this.pagine.length - 2) && (i === this.pagine.length - 1 || i === this.pagine.length - 2 || i === this.pagine.length - 3)) {
      return true;
    }
    if (i === index || i === index + 1 || i === index + 2) {
      return true;
    }
    return false;
  }

  public changePage(i: number): void {
    this.changeActivePage(i);
  }

  nextPage() {
    const index = this.pagine.findIndex(item => item.active);
    if (index !== this.pagine.length - 1) {
      this.changeActivePage(index + 1);
    }
  }

  previousPage() {
    const index = this.pagine.findIndex(item => item.active);
    if (index !== 0) {
      this.changeActivePage(index - 1);
    }
  }

  changeActivePage(newIndex: number) {
    const index = this.pagine.findIndex(item => item.active);
    this.pagine[index].active = false;
    this.pagine[newIndex].active = true;
    this.showNextDots();
    this.showPreviousDots();
    this.onChangePage.emit(newIndex);
  }

  setIconFilter(sortType: string, columnName: string) {
    this.columns.find(c => c.name === columnName)!.sortType = sortType;
  }

  clearFilter(columnName: string) {
    this.setIconFilter('Asc', columnName);
    this.onClearFilter.emit(columnName);
  }

  downFilter(columnName: string) {
    this.setIconFilter('Both', columnName);
    this.onDownFilter.emit();
  }

  upFilter(columnName: string) {
    this.setIconFilter('Desc', columnName);
    this.onUpFilter.emit(columnName);
  }

  selectRow(row: ITableRow) {
    this.onSelectRow.emit(row);
  }

  edit(e: any, row: ITableRow) {
    e.stopPropagation();
    e.preventDefault();
    this.onEditRow.emit(row);
  }

  delete(e: any, row: ITableRow) {
    e.stopPropagation();
    e.preventDefault();
    Swal.fire({
      title: 'Sei sicuro?',
      text: "Non potrai tornare indietro!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, elimina!',
      cancelButtonText: 'No, annulla!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.onDeleteRow.emit(row);
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annullato',
          'Operazione annullata correttamente',
          'error'
        );
      }
    });
  }

  caricoMagazzino(e: any, row: ITableRow) {
    e.stopPropagation();
    e.preventDefault();
    this.onCaricoRow.emit(row);
  }
}
