import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { StatisticsService } from 'src/app/services/statistics.service';

@Component({
  selector: 'app-sellings-charts',
  templateUrl: './sellings-charts.component.html'
})
export class SellingsChartsComponent implements OnInit {

  private readonly dateFormat = 'yyyy-MM-dd';

  ordersData = [
    {
      "name": "Loading...",
      "value": 0
    }
  ];

  itemsDataCount = 0;
  itemsData = [
    {
      "name": "Loading...",
      "value": 0
    }
  ];

  categoriesDataCount = 0;
  categoriesData = [
    {
      "name": "Loading...",
      "value": 0
    }
  ];

  gradient = true;
  showLabels = true;
  isDoughnut = false;

  dateSelectionStart: Date | null | string = null;

  public constructor(private readonly statisticsService: StatisticsService, private readonly datePipe: DatePipe) { }

  public ngOnInit(): void {
    this.dateSelectionChanged();
  }

  /**
   * Adds 7 days
   */
  get dateSelectionEnd(): Date {
    const newDate = new Date(this.dateSelectionStart!);
    newDate.setDate(newDate.getDate() + 7 + 1);
    return newDate;
  }

  get ordersDataSum(): number {
    return this.ordersData.reduce((sum, d) => sum + d.value, 0);
  }

  get itemsDataSum(): number {
    return this.itemsData.reduce((sum, d) => sum + d.value, 0);
  }

  get categoriesDataSum(): number {
    return this.categoriesData.reduce((sum, d) => sum + d.value, 0);
  }

  public dateSelectionChanged(): void {
    this.dateSelectionStart = this.dateSelectionStart === "" ? null : this.dateSelectionStart;

    const orders = this.dateSelectionStart !== null ?
      this.statisticsService.orders(
        this.datePipe.transform(this.dateSelectionStart, this.dateFormat)!,
        this.datePipe.transform(this.dateSelectionEnd, this.dateFormat)!
      ) : this.statisticsService.ordersAll();

    const items = this.dateSelectionStart !== null ?
      this.statisticsService.items(
        this.datePipe.transform(this.dateSelectionStart, this.dateFormat)!,
        this.datePipe.transform(this.dateSelectionEnd, this.dateFormat)!
      ) : this.statisticsService.itemsAll();

    const categories = this.dateSelectionStart !== null ?
      this.statisticsService.categories(
        this.datePipe.transform(this.dateSelectionStart, this.dateFormat)!,
        this.datePipe.transform(this.dateSelectionEnd, this.dateFormat)!
      ) : this.statisticsService.categoriesAll();

    orders.subscribe(r => {
      this.ordersData = r.map(res => {
        return { name: res[0], value: res[1] };
      });
    });

    items.subscribe(r => {
      this.itemsDataCount = 0;
      this.itemsData = r.map(res => {
        this.itemsDataCount += res[2];
        return { name: res[0], value: res[1] };
      });
    });

    categories.subscribe(r => {
      this.categoriesDataCount = 0;
      this.categoriesData = r.map(res => {
        this.categoriesDataCount += res[2];
        return { name: res[0], value: res[1] };
      });
    });
  }

}
