import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellingsChartsComponent } from './sellings-charts.component';

describe('SellingsChartsComponent', () => {
  let component: SellingsChartsComponent;
  let fixture: ComponentFixture<SellingsChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellingsChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingsChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
