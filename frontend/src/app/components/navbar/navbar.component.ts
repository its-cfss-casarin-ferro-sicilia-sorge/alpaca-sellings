import { Component, Input, OnInit } from '@angular/core';
import { faBars, faCartArrowDown, faCartPlus, faHouseUser, faSignOutAlt, faTruckMoving, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import IPageNav from 'src/app/interfaces/IPageNav';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  faCartPlus = faCartPlus;
  faCartArrowDown = faCartArrowDown;
  faTrukMoving = faTruckMoving;
  faWarehouse = faWarehouse;
  faBars = faBars;
  faHouseUser = faHouseUser;
  faSignOutAlt = faSignOutAlt;
  public pageActiveName = "";

  @Input()
  public pages: IPageNav[] = [];

  ngOnInit(): void {
    this.pageActiveName = this.pages[this.pages.findIndex(item => item.active)].name;
  }

}
