import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import IFormCU from 'src/app/interfaces/IFormCU';

@Component({
  selector: 'app-formcu',
  templateUrl: './formcu.component.html',
  styleUrls: ['./formcu.component.scss']
})
export class FormcuComponent implements OnInit {

  public groupForm: FormGroup;
  public label = "Non attivo";
  public faCircleNotch = faCircleNotch;

  @Input()
  public lista: IFormCU[] = []

  @Input()
  public buttonSalva = true;

  @Input()
  public loading = true;

  @Input()
  public tipo?: string;

  @Input()
  public nomeButton = 'SALVA';

  @Output()
  onSave: EventEmitter<FormGroup> = new EventEmitter();

  constructor() {
    this.groupForm = this.toFormGroup(this.lista);
  }

  ngOnInit(): void {
    this.groupForm = this.toFormGroup(this.lista);
  }

  toFormGroup(form: IFormCU[]) {
    const group: any = {};

    form.forEach(ele => {
      group[ele.name] = new FormControl({ value: ele.value || '', disabled: ele.disabled || false }, Validators.required);
      if (ele.type === 'checkbox' && this.tipo === 'Acquisto') {
        this.label = ele.value == true ? 'Attivo' : 'Non attivo';
      }
    });

    return new FormGroup(group);
  }

  onSubmit() {
    this.groupForm.markAllAsTouched();
    if (this.groupForm.valid || this.tipo === 'Acquisto') {
      this.onSave.emit(this.groupForm.value);
    }
  }

  changeCheck(name: string) {
    const obj: any = {}
    obj[name] = this.groupForm.value[name] === "true" ? false : true;
    this.label = obj[name] ? 'Attivo' : 'Non attivo';
    this.groupForm.patchValue(obj);
  }
}
