import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import IFormCU from 'src/app/interfaces/IFormCU';
import IPurchase from 'src/app/interfaces/IPurchase';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {

  public datiVendita?: IPurchase;
  public formAcquisto = true;
  public formAcquistoItem = false;
  public showForm = true;
  public storicoArticoli: any[] = [];

  @Input()
  public loading = true;

  @Input()
  public show = true;

  @Input()
  public type = "Nuovo";

  @Input()
  public totalPages = 1;

  @Input()
  public articoli: { value: any, label: string }[] = [];

  @Input()
  public campiCRU: IFormCU[] = [];

  @Input()
  public campiAcquisto: IFormCU[] = []

  @Input()
  public columns: ITableColumn[] = [];

  @Input()
  public rows: ITableRow[] = [];

  @Output()
  onEditPurchase: EventEmitter<any> = new EventEmitter();

  @Output()
  onNewPurchase: EventEmitter<any> = new EventEmitter();

  constructor(private readonly itemServie: ItemService) { }

  ngOnInit(): void {
  }

  continuaAcquisto(event: any) {
    if (this.totalPages === undefined || this.totalPages === 0) this.totalPages = 1;
    this.datiVendita = event;
    this.changePage(false, true);
  }

  changePage(acquisti: boolean, acquistiItem: boolean) {
    this.formAcquisto = acquisti;
    this.formAcquistoItem = acquistiItem;
    this.campiAcquisto.forEach(e => {
      e.disabled = this.type === 'Visualizza' ? true : false;
      if (this.type === 'Nuovo') e.value = '';
    });
    if (this.type === 'Nuovo') this.rows = [];
    if (acquistiItem)
      this.rows.forEach(e => {
        this.storicoArticoli.push({
          asin: e.cells[0].data,
          quantityPurchase: e.cells[1].data,
          unitPurchasePrice: e.cells[2].data
        })
      });
  }

  aggiungiItem(event: any) {
    let indice = this.storicoArticoli.findIndex(c => c.asin === event.asin);
    let i = 0;
    this.rows.forEach(e => {
      if (e.cells[0].data === event.asin)
        this.rows.splice(i, 1);
      i++;
    });
    this.itemServie.getPrezzoByAsin(event.asin).subscribe((res) => {
      if (indice === -1) {
        this.storicoArticoli.push({
          asin: event.asin,
          quantityPurchase: event.quantityPurchase,
          unitPurchasePrice: (res * event.quantityPurchase),
          nuovo: true
        })
      }
      else {
        this.storicoArticoli[indice].asin = event.asin;
        this.storicoArticoli[indice].quantityPurchase = event.quantityPurchase;
        this.storicoArticoli[indice].unitPurchasePrice = event.unitPurchasePrice;
        this.storicoArticoli[indice].modifica = true;
      }
      this.rows.push({
        cells: [
          { data: event.asin },
          { data: event.quantityPurchase.toString() },
          { data: (res * event.quantityPurchase).toString() },
          {
            data: 'button',
            type: 'button'
          },
        ]
      })
      this.campiAcquisto.forEach(e => {
        e.value = ''
      });
      this.showForm = false;
      setTimeout(() => {
        this.showForm = true;
      }, 100);
    })
  }

  removeItem(event: any) {
    this.rows.splice(this.rows.findIndex(c => c === event), 1);
    this.storicoArticoli[this.storicoArticoli.findIndex(c => c.asin === event.cells[0].data)].elimina = true;
  }

  confermaAcquisto() {
    if (this.type === 'Nuovo')
      this.onNewPurchase.emit({ purchase: this.datiVendita, item: this.storicoArticoli });
    else
      this.onEditPurchase.emit({ purchase: this.datiVendita, item: this.storicoArticoli });
  }

}
