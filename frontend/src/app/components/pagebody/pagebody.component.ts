import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import IFormCU from 'src/app/interfaces/IFormCU';
import IPageNav from 'src/app/interfaces/IPageNav';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';

@Component({
  selector: 'app-pagebody',
  templateUrl: './pagebody.component.html',
  styleUrls: ['./pagebody.component.scss']
})
export class PagebodyComponent implements OnInit {

  public titleTab = "Nuovo";
  public buttonSalva = true;
  public tabLista = true;
  public tabCreateReadUpdate = false;
  public campiCreate: IFormCU[] = [];
  public campiReadUpdate: IFormCU[] = [];

  @Input()
  public nameTitle = "";

  @Input()
  public totalPages = 1;

  @Input()
  public totalPagesArticoli = 1;

  @Input()
  public loading = true;

  @Input()
  public show = true;

  @Input()
  public caricoMagazzinoButton = false;

  @Input()
  public tipo?: string;

  @Input()
  public listaArticoli: { value: any, label: string }[] = [];

  @Input()
  public pages: IPageNav[] = [];

  @Input()
  public campiCRU: IFormCU[] = [];

  @Input()
  public campiAcquisto: IFormCU[] = [];

  @Input()
  public columns: ITableColumn[] = [];

  @Input()
  public rows: ITableRow[] = [];

  @Input()
  public columnsArticoli: ITableColumn[] = [];

  @Input()
  public rowsArticoli: ITableRow[] = [];

  @Output()
  onEditItem: EventEmitter<any> = new EventEmitter();

  @Output()
  onGetFieldItem: EventEmitter<ITableRow> = new EventEmitter();

  @Output()
  onNewItem: EventEmitter<any> = new EventEmitter();

  @Output()
  onDeleteRow: EventEmitter<ITableRow> = new EventEmitter();

  @Output()
  onChangePage: EventEmitter<number> = new EventEmitter();

  @Output()
  onClearFilter: EventEmitter<string> = new EventEmitter();

  @Output()
  onDownFilter: EventEmitter<void> = new EventEmitter();

  @Output()
  onUpFilter: EventEmitter<string> = new EventEmitter();

  @Output()
  onNewPurchase: EventEmitter<any> = new EventEmitter();

  @Output()
  onEditPurchase: EventEmitter<any> = new EventEmitter();

  @Output()
  onCaricoRow: EventEmitter<ITableRow> = new EventEmitter();

  ngOnInit(): void {
    this.campiReadUpdate = [...this.campiCRU];
    const myArray = [...this.campiCRU];
    myArray.splice(0, 1)
    this.campiCreate = myArray;
    if (this.titleTab === 'Nuovo' && this.nameTitle !== 'Articolo') {
      this.campiCRU = this.campiCreate;
    }
  }

  page(lista: boolean, createReadUpdate: boolean) {
    this.tabLista = lista;
    this.tabCreateReadUpdate = createReadUpdate;
    if (this.tabLista) {
      this.cambioTab("Nuovo", false, true, false);
    }
  }

  cambioTab(title: string, disabled: boolean, buttonSalva: boolean, loading: boolean) {
    this.titleTab = title;
    if (this.titleTab === 'Nuovo' && this.nameTitle !== 'Articolo') {
      this.campiCRU = this.campiCreate;
    }
    else {
      this.campiCRU = this.campiReadUpdate;
    }
    this.campiCRU.forEach(e => {
      if (this.titleTab === 'Nuovo') {
        e.value = '';
      }
      e.disabled = disabled;
    });
    if (this.titleTab === 'Modifica') {
      this.campiCRU[0].readonly = true;
    }
    this.buttonSalva = buttonSalva;
    this.loading = loading;
  }

  selectRow(event: ITableRow) {
    this.cambioTab("Visualizza", true, false, true);
    this.page(false, true);
    this.onGetFieldItem.emit(event);
  }

  deleteRow(event: ITableRow) {
    this.onDeleteRow.emit(event);
  }

  editRow(event: ITableRow) {
    this.cambioTab("Modifica", false, true, true);
    this.page(false, true);
    this.onGetFieldItem.emit(event);
  }

  saveInput(event: any) {
    if (this.titleTab === 'Modifica') {
      this.onEditItem.emit(event)
    }
    else if (this.titleTab === 'Nuovo') {
      this.onNewItem.emit(event)
    }
  }

  changePage(event: number) {
    this.onChangePage.emit(event);
  }

  clearFilter(columnName: string) {
    this.onClearFilter.emit(columnName);
  }

  downFilter() {
    this.onDownFilter.emit();
  }

  upFilter(columnName: string) {
    this.onUpFilter.emit(columnName);
  }

  nuovoAcquisto(event: any) {
    this.onNewPurchase.emit(event);
  }

  editPurchase(event: any) {
    this.onEditPurchase.emit(event);
  }

  caricoMagazzino(event: any) {
    this.onCaricoRow.emit(event);
  }
}
