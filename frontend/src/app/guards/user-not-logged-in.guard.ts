import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import * as Cookies from 'js-cookie';

@Injectable({
  providedIn: 'root'
})
export class UserNotLoggedInGuard implements CanActivate {

  constructor(private readonly router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (Cookies.get('user') !== undefined) {
      this.router.navigate(['home']);
    }
    return true;
  }

}
