import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { TableComponent } from './components/table/table.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TokenInterceptor } from './services/token-interceptor';
import { SellingsChartsComponent } from './components/sellings-charts/sellings-charts.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MagazzinoComponent } from './pages/magazzino/magazzino.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormcuComponent } from './components/formcu/formcu.component';
import { PagebodyComponent } from './components/pagebody/pagebody.component';
import { FornitoreComponent } from './pages/fornitore/fornitore.component';
import { AcquistoComponent } from './pages/acquisto/acquisto.component';
import { VenditaComponent } from './pages/vendita/vendita.component';
import { DatePipe } from '@angular/common';
import { PurchaseComponent } from './components/purchase/purchase.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    LogoutComponent,
    TableComponent,
    SellingsChartsComponent,
    MagazzinoComponent,
    NavbarComponent,
    FormcuComponent,
    PagebodyComponent,
    FornitoreComponent,
    AcquistoComponent,
    VenditaComponent,
    PurchaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    NgxChartsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
