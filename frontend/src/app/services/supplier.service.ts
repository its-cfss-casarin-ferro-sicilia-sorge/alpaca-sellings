import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import ISupplier from '../interfaces/ISupplier';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'supplierId,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.supplier}?page=${pageNum}&size=1000&sort=${sort}`);
  }

  public getById(id: number) {
    return this.http.get<ISupplier>(`${environment.backendUrl}${environment.services.supplier}/${id}`);
  }

  public insertSupplier(item: ISupplier) {
    return this.http.post<boolean>(`${environment.backendUrl}${environment.services.supplier}`, item );
  }

  public updateSupplier(item: ISupplier) {
    return this.http.put<boolean>(`${environment.backendUrl}${environment.services.supplier}`, item);
  }

  public deleteSupplier(id: number) {
    return this.http.delete<boolean>(`${environment.backendUrl}${environment.services.supplier}/${id}`);
  }
}
