import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import IOrderItem from '../interfaces/IOrderItem';

@Injectable({
  providedIn: 'root'
})
export class OrderItemService {

  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'orderItemId,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.orderitem}?page=${pageNum}&size=10&sort=${sort}`);
  }

  public getById(id: string) {
    return this.http.get<IOrderItem>(`${environment.backendUrl}${environment.services.orderitem}/${id}`);
  }

  public getByAmazonOrderID(id: string, pageNum: number = 0, sort: string = 'asin,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.orderitem}/amazonOrderId/${id}?page=${pageNum}&size=10&sort=${sort}`);
  }
}
