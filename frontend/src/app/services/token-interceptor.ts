import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import * as Cookies from 'js-cookie';

export class TokenInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const user = Cookies.getJSON('user');

    if (user?.token) {
      const tokenizedReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`
        }
      });

      return next.handle(tokenizedReq);
    }

    return next.handle(req);
  }
}
