import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import IPurchase from '../interfaces/IPurchase';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'purchaseId,asc', size: number = 10) {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.purchase}?page=${pageNum}&size=${size}&sort=${sort}`);
  }

  public getById(id: number) {
    return this.http.get<IPurchase>(`${environment.backendUrl}${environment.services.purchase}/${id}`);
  }

  public insertItem(item: IPurchase) {
    return this.http.post<boolean>(`${environment.backendUrl}${environment.services.purchase}`, item );
  }

  public updateItem(item: IPurchase) {
    return this.http.put<boolean>(`${environment.backendUrl}${environment.services.purchase}`, item );
  }

  public deleteItem(id: number) {
    return this.http.delete<boolean>(`${environment.backendUrl}${environment.services.purchase}/${id}`);
  }
}
