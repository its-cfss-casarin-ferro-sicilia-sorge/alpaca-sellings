import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'amazonOrderId,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.order}?page=${pageNum}&size=10&sort=${sort}`);
  }

  public getById(id: string) {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.order}/${id}`);
  }
}
