import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import LoginResponse from '../interfaces/LoginResponse';
import * as Cookies from 'js-cookie';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public username?: string;

  constructor(private readonly http: HttpClient, private readonly router: Router) { }

  public login(username: string, password: string) {
    return this.http.post<LoginResponse>(`${environment.backendUrl}${environment.services.user}/login`, {
      username,
      password
    }
    );
  }

  public logout(): void {
    Cookies.remove('user');
    this.router.navigate(['login']);
  }
}
