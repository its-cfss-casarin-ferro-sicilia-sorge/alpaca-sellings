import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import IPurchaseItem from '../interfaces/IPurchaseItem';

@Injectable({
  providedIn: 'root'
})
export class PurchaseItemService {

  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'asin,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.purchaseitem}?page=${pageNum}&size=10&sort=${sort}`);
  }

  public getByPurchaseId(id: number, pageNum: number = 0, sort: string = 'asin,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.purchaseitem}/purchaseid/${id}?page=${pageNum}&size=10&sort=${sort}`);
  }

  public getById(asin: string, purchaseID: number) {
    return this.http.get<IPurchaseItem>(`${environment.backendUrl}${environment.services.purchaseitem}/${asin}/${purchaseID}`);
  }

  public warehouseLoading(purchaseID: number) {
    return this.http.get<boolean>(`${environment.backendUrl}${environment.services.purchaseitem}/warehouseLoading/${purchaseID}`);
  }

  public insertItem(item: IPurchaseItem) {
    return this.http.post<boolean>(`${environment.backendUrl}${environment.services.purchaseitem}`, item);
  }

  public updateItem(item: IPurchaseItem) {
    return this.http.put<boolean>(`${environment.backendUrl}${environment.services.purchaseitem}`, item);
  }

  public deleteItem(asin: string, purchaseID: number) {
    return this.http.delete<boolean>(`${environment.backendUrl}${environment.services.purchaseitem}/${asin}/${purchaseID}`);
  }
}
