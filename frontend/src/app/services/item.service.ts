import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import IItem from '../interfaces/IItem';

@Injectable({
  providedIn: 'root'
})
export class ItemService {


  constructor(private readonly http: HttpClient) { }

  public getAll(pageNum: number = 0, sort: string = 'asin,asc') {
    return this.http.get<any>(`${environment.backendUrl}${environment.services.item}?page=${pageNum}&size=10&sort=${sort}`);
  }

  public getByAsin(asin: string) {
    return this.http.get<IItem>(`${environment.backendUrl}${environment.services.item}/${asin}`);
  }

  public getPrezzoByAsin(asin: string) {
    return this.http.get<number>(`${environment.backendUrl}${environment.services.item}/prezzo/${asin}`);
  }

  public insertItem(item: IItem) {
    return this.http.post<boolean>(`${environment.backendUrl}${environment.services.item}`, item );
  }

  public updateItem(item: IItem) {
    return this.http.put<boolean>(`${environment.backendUrl}${environment.services.item}`, item );
  }

  public deleteItem(asin: string) {
    return this.http.delete<boolean>(`${environment.backendUrl}${environment.services.item}/${asin}`);
  }
}
