import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private readonly http: HttpClient) { }

  public orders(start: string, end: string) {
    return this.http.get<[string, number][]>(`${environment.backendUrl}${environment.services.statistics}/orders/${start}/${end}`);
  }

  public items(start: string, end: string) {
    return this.http.get<[string, number, number][]>(`${environment.backendUrl}${environment.services.statistics}/asins/${start}/${end}`);
  }

  public categories(start: string, end: string) {
    return this.http.get<[string, number, number][]>(`${environment.backendUrl}${environment.services.statistics}/categories/${start}/${end}`);
  }

  public ordersAll() {
    return this.http.get<[string, number][]>(`${environment.backendUrl}${environment.services.statistics}/orders`);
  }

  public itemsAll() {
    return this.http.get<[string, number, number][]>(`${environment.backendUrl}${environment.services.statistics}/asins`);
  }

  public categoriesAll() {
    return this.http.get<[string, number, number][]>(`${environment.backendUrl}${environment.services.statistics}/categories`);
  }

}
