import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import IFormCU from 'src/app/interfaces/IFormCU';
import IItem from 'src/app/interfaces/IItem';
import IOrder from 'src/app/interfaces/IOrder';
import IOrderItem from 'src/app/interfaces/IOrderItem';
import IPageNav from 'src/app/interfaces/IPageNav';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { ItemService } from 'src/app/services/item.service';
import { OrderItemService } from 'src/app/services/order-item.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-vendita',
  templateUrl: './vendita.component.html',
  styleUrls: ['./vendita.component.scss'],
})
export class VenditaComponent implements OnInit {

  public tabLista = true;
  public tabDettaglio = false;
  public tabAnalisi = false;
  public tabArticoli = false;
  public visualizzaDettaglio = false;
  public dettaglioArticolo = false;
  public loading = true;
  public mostraBody = true;
  public firstLoading = true;
  public totalPages = 1;
  public mostraBodyArticoli = true;
  public firstLoadingArticoli = true;
  public totalPagesArticoli = 1;
  public orderSelected = "";
  public itemSelected?: IItem;
  public pages: IPageNav[] = [
    {
      name: 'Home',
      link: 'home'
    },
    {
      name: 'Vendite',
      active: true
    }
  ]
  public campiCRU: IFormCU[] = [
    {
      name: 'AmazonOrderId',
      label: 'Amazon ID di vendita',
      type: 'text',
      disabled: true
    },
    {
      name: 'PurchaseDate',
      label: 'Data di acquisto',
      type: 'date',
      disabled: true
    },
    {
      name: 'LastUpdateDate',
      label: 'Ultima data di aggiornamento',
      type: 'date',
      disabled: true
    },
    {
      name: 'OrderStatus',
      label: 'Stato dell\'ordine',
      type: 'text',
      disabled: true
    },
    {
      name: 'FulfillmentChannel',
      label: 'Canale di adempimento',
      type: 'text',
      disabled: true
    },
    {
      name: 'NumberOfItemsShipped',
      label: 'Numero di articoli spediti',
      type: 'number',
      disabled: true
    },
    {
      name: 'NumberOfItemsUnshipped',
      label: 'Numero di articoli non spediti',
      type: 'number',
      disabled: true
    },
    {
      name: 'PaymentMethod',
      label: 'Metodo di pagamento',
      type: 'text',
      disabled: true
    },
    {
      name: 'PaymentMethodDetails',
      label: 'Dettagli del metodo di pagamento',
      type: 'text',
      disabled: true
    },
    {
      name: 'MarketplaceID',
      label: 'ID marketplace',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShipmentServiceLevelCategory',
      label: 'Categoria del livello di servizio di spedizione',
      type: 'text',
      disabled: true
    },
    {
      name: 'OrderType',
      label: 'Tipo di vendita',
      type: 'text',
      disabled: true
    },
    {
      name: 'EarliestShipDate',
      label: 'Prima data di spedizione',
      type: 'date',
      disabled: true
    },
    {
      name: 'LatestShipDate',
      label: 'Ultima data di spedizione',
      type: 'date',
      disabled: true
    },
    {
      name: 'IsBusinessOrder',
      label: 'E\' un ordine commerciale',
      type: 'checkbox',
      disabled: true
    },
    {
      name: 'IsPrime',
      label: 'E\' prime',
      type: 'checkbox',
      disabled: true
    },
    {
      name: 'IsGlobalExpressEnabled',
      label: 'E\' abilitato al global express',
      type: 'checkbox',
      disabled: true
    },
    {
      name: 'IsPremiumOrder',
      label: 'E\' un odine premium',
      type: 'checkbox',
      disabled: true
    },
    {
      name: 'IsSoldByAB',
      label: 'E\' venduto da ab',
      type: 'checkbox',
      disabled: true
    },
    {
      name: 'CompanyLegalName',
      label: 'Denominazione azienda',
      type: 'text',
      disabled: true
    },
    {
      name: 'BuyerEmail',
      label: 'Email compratore',
      type: 'text',
      disabled: true
    },
    {
      name: 'PurchaseOrderNumber',
      label: 'Numero dell\'ordine d\'acquisto',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShippingAddressName',
      label: 'Nome indirizzo di spedizione',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShippingAddressLine1',
      label: 'Riga indirizzo di spedizione',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShippingAddressCity',
      label: 'Città dell\'indirizzo di spedizione',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShippingCityStateOrRegion',
      label: 'Città stato o regione di spedizione',
      type: 'text',
      disabled: true
    },
    {
      name: 'ShippingStateOrRegionPostalCode',
      label: 'Codice postale dello stato o della regione di spedizione',
      type: 'text',
      disabled: true
    }
  ]

  public columns: ITableColumn[] = [
    {
      title: 'ID',
      name: 'AmazonOrderId',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Stato dell\'ordine',
      name: 'OrderStatus',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'NumberOfItemsShipped',
      title: 'Articoli spediti',
      type: 'number',
      sortType: 'Both'
    },
    {
      name: 'NumberOfItemsUnshipped',
      title: 'Articoli non spediti',
      type: 'number',
      sortType: 'Both'
    },
    {
      title: 'Email compratore',
      name: 'BuyerEmail',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
  ];

  public rows: ITableRow[] = [];

  public columnsArticoli: ITableColumn[] = [
    {
      title: 'ASIN',
      name: 'asin',
      type: 'text',
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Titolo',
      name: 'title',
      type: 'text',
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Quantità ordinata',
      name: 'quantityOrdered',
      type: 'number',
    },
    {
      name: 'quantityShipped',
      title: 'Quantità spedita',
      type: 'number',
    },
    {
      name: 'shippingPriceAmount',
      title: 'Prezzo totale',
      type: 'number',
      sortable: true,
      sortType: 'Both'
    }
  ];

  public rowsArticoli: ITableRow[] = [];

  constructor(private readonly orderService: OrderService, private readonly datepipe: DatePipe,
    private readonly orderItemService: OrderItemService, private readonly itemService: ItemService) { }

  ngOnInit(): void {
    this.caricaOrdini();
  }

  caricaOrdini(pageNum: number = 0, sort: string = 'amazonOrderId,asc') {
    this.rows = [];
    this.orderService.getAll(pageNum, sort).subscribe((res) => {
      this.totalPages = res.totalPages;
      res.content.forEach((e: IOrder) => {
        this.rows.push({
          cells: [
            { data: e.amazonOrderId },
            { data: e.orderStatus },
            { data: e.numberOfItemsShipped.toString() },
            { data: e.numberOfItemsUnshipped.toString() },
            { data: e.buyerEmail }
          ]
        });
      });
      if (this.firstLoading){
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
        this.firstLoading = false;
      }
    })
  }

  page(lista: boolean, analisi: boolean, dettaglio: boolean, articoli: boolean, visualizzaDettaglio: boolean) {
    this.tabAnalisi = analisi;
    this.tabLista = lista;
    this.tabDettaglio = dettaglio;
    this.tabArticoli = articoli;
    this.visualizzaDettaglio = visualizzaDettaglio;
  }

  selectRow(event: ITableRow) {
    this.visualizzaDettaglio = true;
    this.loading = true;
    this.firstLoading = true;
    this.rowsArticoli = [];
    this.orderSelected = "";
    const dateFormat = 'yyyy-MM-dd';
    this.orderService.getById(event.cells[0].data).subscribe((res) => {
      this.campiCRU.find(c => c.name === 'AmazonOrderId')!.value = res.AmazonOrderId;
      this.campiCRU.find(c => c.name === 'PurchaseDate')!.value = this.datepipe.transform(res.PurchaseDate, dateFormat);
      this.campiCRU.find(c => c.name === 'LastUpdateDate')!.value = this.datepipe.transform(res.LastUpdateDate, dateFormat);
      this.campiCRU.find(c => c.name === 'OrderStatus')!.value = res.OrderStatus;
      this.campiCRU.find(c => c.name === 'FulfillmentChannel')!.value = res.FulfillmentChannel;
      this.campiCRU.find(c => c.name === 'NumberOfItemsShipped')!.value = res.NumberOfItemsShipped.toString();
      this.campiCRU.find(c => c.name === 'NumberOfItemsUnshipped')!.value = res.NumberOfItemsUnshipped.toString();
      this.campiCRU.find(c => c.name === 'PaymentMethod')!.value = res.PaymentMethod;
      this.campiCRU.find(c => c.name === 'PaymentMethodDetails')!.value = res.PaymentMethodDetails;
      this.campiCRU.find(c => c.name === 'MarketplaceID')!.value = res.MarketplaceId;
      this.campiCRU.find(c => c.name === 'ShipmentServiceLevelCategory')!.value = res.ShipmentServiceLevelCategory;
      this.campiCRU.find(c => c.name === 'OrderType')!.value = res.OrderType;
      this.campiCRU.find(c => c.name === 'EarliestShipDate')!.value = this.datepipe.transform(res.EarliestShipDate, dateFormat);
      this.campiCRU.find(c => c.name === 'LatestShipDate')!.value = this.datepipe.transform(res.LatestShipDate, dateFormat);
      this.campiCRU.find(c => c.name === 'IsBusinessOrder')!.value = res.IsBusinessOrder;
      this.campiCRU.find(c => c.name === 'IsPrime')!.value = res.IsPrime;
      this.campiCRU.find(c => c.name === 'IsGlobalExpressEnabled')!.value = res.IsGlobalExpressEnabled;
      this.campiCRU.find(c => c.name === 'IsPremiumOrder')!.value = res.IsPremiumOrder;
      this.campiCRU.find(c => c.name === 'IsSoldByAB')!.value = res.IsSoldByAB;
      this.campiCRU.find(c => c.name === 'CompanyLegalName')!.value = res.CompanyLegalName;
      this.campiCRU.find(c => c.name === 'BuyerEmail')!.value = res.BuyerEmail;
      this.campiCRU.find(c => c.name === 'PurchaseOrderNumber')!.value = res.PurchaseOrderNumber;
      this.campiCRU.find(c => c.name === 'ShippingAddressName')!.value = res.ShippingAddressName;
      this.campiCRU.find(c => c.name === 'ShippingAddressLine1')!.value = res.ShippingAddressLine1;
      this.campiCRU.find(c => c.name === 'ShippingAddressCity')!.value = res.ShippingAddressCity;
      this.campiCRU.find(c => c.name === 'ShippingCityStateOrRegion')!.value = res.ShippingCityStateOrRegion;
      this.campiCRU.find(c => c.name === 'ShippingStateOrRegionPostalCode')!.value = res.ShippingStateOrRegionPostalCode;
      this.orderSelected = res.AmazonOrderId;
      this.caricaArticoli();
      setTimeout(() => {
        this.loading = false;
        this.page(false, false, true, false, true)
      }, 100);
    })
  }

  selectRowArticoli(event: ITableRow) {
    this.itemService.getByAsin(event.cells[0].data).subscribe((res) => {
      this.itemSelected = res;
      this.dettaglioArticolo = true;
    })
  }

  caricaArticoli(pageNum: number = 0, sort: string = 'asin,asc') {
    this.orderItemService.getByAmazonOrderID(this.orderSelected, pageNum, sort).subscribe((res) => {
      this.rowsArticoli = [];
      if (res.content != null){
        res.content.forEach((e: IOrderItem) => {
          this.rowsArticoli.push({
            cells: [
              { data: e.ASIN },
              { data: e.Title },
              { data: e.QuantityOrdered.toString() },
              { data: e.QuantityShipped.toString() },
              { data: e.ShippingPriceAmount.toString() }
            ]
          });
        });
      }
      if (this.firstLoadingArticoli) {
        this.mostraBodyArticoli = false;
        setTimeout(() => {
          this.mostraBodyArticoli = true;
        }, 100);
        this.firstLoadingArticoli = false;
      }
    })
  }

  changePage(event: number, tipo: string) {
    tipo === 'Ordini' ? this.caricaOrdini(event) : this.caricaArticoli(event);
  }

  clearFilter(columnName: string, tipo: string) {
    tipo === 'Ordini' ? this.caricaOrdini(0, columnName + ',asc') : this.caricaArticoli(0, columnName + ',asc');
  }

  downFilter(tipo: string) {
    tipo === 'Ordini' ? this.caricaOrdini() : this.caricaArticoli();
  }

  upFilter(columnName: string, tipo: string) {
    tipo === 'Ordini' ? this.caricaOrdini(0, columnName + ',desc') : this.caricaArticoli(0, columnName + ',desc');
  }
}
