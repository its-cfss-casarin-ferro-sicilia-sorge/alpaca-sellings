import { Component } from '@angular/core';
import { faCartArrowDown, faCartPlus, faTruckMoving, faWarehouse, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  faCartPlus = faCartPlus;
  faCartArrowDown = faCartArrowDown;
  faTrukMoving = faTruckMoving;
  faWarehouse = faWarehouse;
  faSignOutAlt = faSignOutAlt;

}
