import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import IFormCU from 'src/app/interfaces/IFormCU';
import IPageNav from 'src/app/interfaces/IPageNav';
import ISupplier from 'src/app/interfaces/ISupplier';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { SupplierService } from 'src/app/services/supplier.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-fornitore',
  templateUrl: './fornitore.component.html'
})
export class FornitoreComponent implements OnInit {

  public loading = false;
  public show = true;
  public mostraBody = true;
  public firstLoading = true;
  public totalPages = 1;
  public pages: IPageNav[] = [
    {
      name: 'Home',
      link: 'home'
    },
    {
      name: 'Fornitori',
      active: true
    }
  ]
  public campiCRU: IFormCU[] = [
    {
      name: 'supplierId',
      label: 'ID',
      type: 'number',
      value: "0"
    },
    {
      name: 'name',
      label: 'Denominazione',
      type: 'text'
    },
    {
      name: 'indirizzo',
      label: 'Indirizzo',
      type: 'text'
    },
    {
      name: 'cellulare',
      label: 'Telefono',
      type: 'text',
      maxlenght: '15',
    },
    {
      name: 'email',
      label: 'Email',
      type: 'text'
    },
    {
      name: 'partitaIva',
      label: 'Partita iva',
      type: 'text',
      maxlenght: '11',
      minlenght: '11'
    }
  ]

  public columns: ITableColumn[] = [
    {
      title: 'ID',
      name: 'supplierId',
      type: 'number',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Denominazione',
      name: 'name',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'cellulare',
      title: 'Telefono',
      type: 'text',
      sortType: 'Both'
    },
    {
      name: 'email',
      title: 'Email',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'button',
      title: '',
      type: 'button',
      sortType: 'Both'
    },
  ];

  public rows: ITableRow[] = [];

  constructor(public readonly supplierService: SupplierService, private readonly router: Router) { }

  ngOnInit(): void {
    this.caricoDati();
  }

  caricoDati(pageNum: number = 0, sort: string = 'supplierId,asc') {
    this.rows = [];
    this.supplierService.getAll(pageNum, sort).subscribe((res) => {
      this.totalPages = res.totalPages;
      res.content.forEach((e: ISupplier) => {
        this.rows.push({
          cells: [
            {
              data: e.supplierId.toString()
            },
            {
              data: e.name
            },
            {
              data: e.cellulare
            },
            {
              data: e.email
            },
            {
              data: 'button',
              type: 'button'
            },
          ]
        });
      });
      if (this.firstLoading) {
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
        this.firstLoading = false;
      }
    })
  }

  getFieldItem(event: ITableRow) {
    this.loading = true;
    this.supplierService.getById(parseInt(event.cells[0].data, 10)).subscribe((res) => {
      this.campiCRU.find(c => c.name === 'supplierId')!.value = res.supplierId.toString();
      this.campiCRU.find(c => c.name === 'name')!.value = res.name;
      this.campiCRU.find(c => c.name === 'cellulare')!.value = res.cellulare;
      this.campiCRU.find(c => c.name === 'email')!.value = res.email;
      this.campiCRU.find(c => c.name === 'indirizzo')!.value = res.indirizzo;
      this.campiCRU.find(c => c.name === 'partitaIva')!.value = res.partitaIva;
      this.loading = false;
      this.show = false;
      setTimeout(() => {
        this.show = true;
      }, 100);
    })
  }

  editItem(event: ISupplier) {
    this.supplierService.updateSupplier(event).subscribe((res) => {
      this.changeTab(res, "Aggiornato");
    })
  }

  changeTab(res: boolean, title: string) {
    if (res) {
      this.caricoDati();
      Swal.fire(title + '!', 'Il fornitore è stato ' + title.toLocaleLowerCase() + ' con successo.', 'success').then(() => {
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
      })
    }
    else{
      Swal.fire('Errore!', 'Il fornitore non è stato ' + title.toLocaleLowerCase() + '.', 'error')
    }
  }

  newItem(event: any) {
    this.supplierService.insertSupplier(event).subscribe((res) => {
      this.changeTab(res, "Inserito");
    })
  }

  deleteRow(event: ITableRow) {
    this.supplierService.deleteSupplier(parseInt(event.cells[0].data, 10)).subscribe((res) => {
      if (res) {
        this.rows.splice(this.rows.findIndex(i => i.cells[0].data === event.cells[0].data), 1)
        Swal.fire('Eliminato!', 'Il fornitore è stato eliminato.', 'success');
      }
      else{
        Swal.fire('Errore!', 'Il fornitore non è stato eliminato.', 'error')
      }
    })
  }

  changePage(event: number) {
    this.caricoDati(event);
  }

  clearFilter(columnName: string) {
    this.caricoDati(0, columnName + ',asc');
  }

  downFilter() {
    this.caricoDati();
  }

  upFilter(columnName: string) {
    this.caricoDati(0, columnName + ',desc');
  }
}
