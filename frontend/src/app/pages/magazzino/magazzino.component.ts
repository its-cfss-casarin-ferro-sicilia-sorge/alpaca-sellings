import { Component, OnInit } from '@angular/core';
import IFormCU from 'src/app/interfaces/IFormCU';
import IItem from 'src/app/interfaces/IItem';
import IPageNav from 'src/app/interfaces/IPageNav';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { ItemService } from 'src/app/services/item.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-magazzino',
  templateUrl: './magazzino.component.html'
})
export class MagazzinoComponent implements OnInit {

  public loading = false;
  public show = true;
  public mostraBody = true;
  public firstLoading = true;
  public totalPages = 1;
  public pages: IPageNav[] = [
    {
      name: 'Home',
      link: 'home'
    },
    {
      name: 'Magazzino',
      active: true
    }
  ]
  public campiCRU: IFormCU[] = [
    {
      name: 'asin',
      label: 'ASIN',
      type: 'text',
      value: ''
    },
    {
      name: 'title',
      label: 'Titolo',
      type: 'text'
    },
    {
      name: 'category',
      label: 'Categoria',
      type: 'text'
    },
    {
      name: 'brand',
      label: 'Brand',
      type: 'text'
    },
    {
      name: 'stock',
      label: 'Quantità',
      type: 'number',
      min: '0'
    },
    {
      name: 'price',
      label: 'Prezzo',
      type: 'number',
      step: '.50',
      min: '0'
    },
    {
      name: 'lowStockLevel',
      label: 'Livello di quantità minima',
      type: 'number'
    },
  ]

  public columns: ITableColumn[] = [
    {
      title: 'ASIN',
      name: 'asin',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Titolo',
      name: 'title',
      type: 'text',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'stock',
      title: 'Quantità',
      type: 'number',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'price',
      title: 'Prezzo',
      type: 'number',
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'button',
      title: '',
      type: 'button',
      sortType: 'Both'
    },
  ];

  public rows: ITableRow[] = [];

  constructor(public readonly itemService: ItemService) { }

  ngOnInit(): void {
    this.caricoDati();
  }

  caricoDati(pageNum: number = 0, sort: string = 'asin,asc'){
    this.rows = [];
    this.itemService.getAll(pageNum, sort).subscribe((res) => {
      this.totalPages = res.totalPages;
      res.content.forEach((e: IItem) => {
        this.rows.push({
          cells: [
            {
              data: e.asin
            },
            {
              data: e.title
            },
            {
              data: e.stock.toString()
            },
            {
              data: e.price.toString()
            },
            {
              data: 'button',
              type: 'button'
            },
          ]
        });
      });
      if (this.firstLoading) {
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
        this.firstLoading = false;
      }
    })
  }

  getFieldItem(event: ITableRow) {
    this.loading = true;
    this.itemService.getByAsin(event.cells[0].data).subscribe((res) => {
      this.campiCRU.find(c => c.name === 'asin')!.value = res.asin;
      this.campiCRU.find(c => c.name === 'brand')!.value = res.brand;
      this.campiCRU.find(c => c.name === 'category')!.value = res.category;
      this.campiCRU.find(c => c.name === 'lowStockLevel')!.value = res.lowStockLevel.toString();
      this.campiCRU.find(c => c.name === 'price')!.value = res.price.toString();
      this.campiCRU.find(c => c.name === 'stock')!.value = res.stock.toString();
      this.campiCRU.find(c => c.name === 'title')!.value = res.title;
      this.loading = false;
      this.show = false;
      setTimeout(() => {
        this.show = true;
      }, 100);
    })
  }

  editItem(event: IItem) {
    this.itemService.updateItem(event).subscribe((res) => {
      this.changeTab(res, "Aggiornato");
    })
  }

  changeTab(res: boolean, title: string) {
    if (res) {
      this.caricoDati();
      Swal.fire(title + '!', 'L\'articolo è stato ' + title.toLocaleLowerCase() + ' con successo.', 'success').then(() => {
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
      })
    }
    else{
      Swal.fire('Errore!', 'L\'articolo non è stato ' + title.toLocaleLowerCase() + '.', 'error')
    }
  }

  newItem(event: IItem) {
    this.itemService.insertItem(event).subscribe((res) => {
      this.changeTab(res, "Inserito");
    })
  }

  deleteRow(event: ITableRow) {
    this.itemService.deleteItem(event.cells[0].data).subscribe((res) => {
      if (res) {
        this.rows.splice(this.rows.findIndex(i => i.cells[0].data === event.cells[0].data), 1)
        Swal.fire('Eliminato!', 'L\'articolo è stato eliminato.', 'success');
      }
      else{
        Swal.fire('Errore!', 'L\'articolo non è stato eliminato.', 'error')
      }
    })
  }

  changePage(event: number) {
    this.caricoDati(event);
  }

  clearFilter(columnName: string) {
    this.caricoDati(0, columnName + ',asc');
  }

  downFilter() {
    this.caricoDati();
  }

  upFilter(columnName: string) {
    this.caricoDati(0, columnName + ',desc');
  }
}
