import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import IFormCU from 'src/app/interfaces/IFormCU';
import IItem from 'src/app/interfaces/IItem';
import IPageNav from 'src/app/interfaces/IPageNav';
import IPurchase from 'src/app/interfaces/IPurchase';
import IPurchaseItem from 'src/app/interfaces/IPurchaseItem';
import ISupplier from 'src/app/interfaces/ISupplier';
import ITableColumn from 'src/app/interfaces/ITableColumn';
import ITableRow from 'src/app/interfaces/ITableRow';
import { ItemService } from 'src/app/services/item.service';
import { PurchaseItemService } from 'src/app/services/purchase-item.service';
import { PurchaseService } from 'src/app/services/purchase.service';
import { SupplierService } from 'src/app/services/supplier.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-acquisto',
  templateUrl: './acquisto.component.html'
})
export class AcquistoComponent implements OnInit {
  public loading = false;
  public show = true;
  public mostraBody = true;
  public totalPages = 0;
  public totalPagesAcquistiItem = 0;
  public supplierSelect: any[] = [];
  public listaArticoli: any[] = [];
  public pages: IPageNav[] = [
    {
      name: 'Home',
      link: 'home'
    },
    {
      name: 'Acquisti',
      active: true
    }
  ]
  public campiCRU: IFormCU[] = [
    {
      name: 'purchaseId',
      label: 'ID',
      type: 'number',
      min: '0',
    },
    {
      name: 'invoiceNumber',
      label: 'Numero fattura',
      type: 'number',
      min: '1'
    },
    {
      name: 'invoiceDate',
      label: 'Data fattura',
      type: 'date'
    },
    {
      name: 'loadCarriedOut',
      label: 'Carico effettuato',
      type: 'checkbox'
    },
    {
      name: 'supplierId',
      label: 'Supplier',
      type: 'select',
      selectValue: this.supplierSelect
    }
  ]

  public columns: ITableColumn[] = [
    {
      title: 'ID',
      name: 'purchaseId',
      type: 'number',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      title: 'Numero fattura',
      name: 'invoiceNumber',
      type: 'number',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'invoiceDate',
      title: 'Data fattura',
      type: 'date',
      searchable: true,
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'loadCarriedOut',
      title: 'Carico',
      type: 'boolean',
      sortable: true,
      sortType: 'Both'
    },
    {
      name: 'button',
      title: '',
      type: 'button',
      sortType: 'Both'
    },
  ];

  public columnsAquistiItem: ITableColumn[] = [
    {
      title: 'ASIN',
      name: 'asin',
      type: 'text',
    },
    {
      title: 'Quantità',
      name: 'quantityPurchase',
      type: 'number',
    },
    {
      name: 'unitPurchasePrice',
      title: 'Prezzo',
      type: 'number',
    },
    {
      name: 'button',
      title: '',
      type: 'button'
    },
  ];

  public campiAcquisto: IFormCU[] = [
    {
      name: 'asin',
      label: 'ASIN',
      type: 'select',
      selectValue: this.listaArticoli
    },
    {
      name: 'quantityPurchase',
      label: 'Quantita da acquistare',
      type: 'number',
      min: '1'
    }
  ]

  public rows: ITableRow[] = [];
  public rowsAcquistiItem: ITableRow[] = [];

  constructor(public readonly purchaseService: PurchaseService, public readonly supplierService: SupplierService,
    private readonly itemService: ItemService, private readonly purchaseItemService: PurchaseItemService, private readonly datepipe: DatePipe) { }

  ngOnInit(): void {
    this.caricoDati();
    this.supplierService.getAll().subscribe((res) => {
      res.content.forEach((e: ISupplier) => {
        this.supplierSelect.push({ label: e.name, value: e.supplierId })
      });
      this.campiCRU.find(c => c.name === 'supplierId')!.value = res.content[0].supplierId;
      this.itemService.getAll().subscribe((res) => {
        res.content.forEach((e: IItem) => {
          this.listaArticoli.push({ label: e.asin + " - " + e.title, value: e.asin })
        });
        this.campiAcquisto.find(c => c.name === 'asin')!.value = res.content[0].asin;
      })
    })
  }

  caricoDati(pageNum: number = 0, sort: string = 'purchaseId,asc') {
    this.rows = [];
    this.purchaseService.getAll(pageNum, sort).subscribe((res) => {
      this.totalPages = res.totalPages;
      res.content.forEach((e: IPurchase) => {
        this.rows.push({
          cells: [
            { data: e.purchaseId.toString() },
            { data: e.invoiceNumber.toString() },
            { data: new Date(e.invoiceDate).toLocaleDateString() },
            { data: e.loadCarriedOut ? 'Effettuato' : 'Non effettuato' },
            {
              data: 'button',
              type: 'button'
            },
          ]
        });
      });
    })
  }

  getFieldItem(event: ITableRow) {
    this.loading = true;
    this.rowsAcquistiItem = [];
    this.listaArticoli = [];
    this.purchaseService.getById(parseInt(event.cells[0].data, 10)).subscribe((res) => {
      this.campiCRU.find(c => c.name === 'purchaseId')!.value = res.purchaseId;
      this.campiCRU.find(c => c.name === 'invoiceNumber')!.value = res.invoiceNumber;
      this.campiCRU.find(c => c.name === 'invoiceDate')!.value = this.datepipe.transform(res.invoiceDate, 'yyyy-MM-dd');
      this.campiCRU.find(c => c.name === 'loadCarriedOut')!.value = res.loadCarriedOut;
      this.campiCRU.find(c => c.name === 'supplierId')!.value = res.supplierId;
      this.loading = false;
      this.show = false;
      setTimeout(() => {
        this.show = true;
      }, 100);
      this.purchaseItemService.getByPurchaseId(parseInt(event.cells[0].data, 10)).subscribe((res) => {
        this.totalPagesAcquistiItem = res.totalPages;
        res.content.forEach((e: IPurchaseItem) => {
          this.rowsAcquistiItem.push({
            cells: [
              { data: e.asin },
              { data: e.quantityPurchased.toString() },
              { data: e.unitPurchasePrice.toString() },
              {
                data: 'button',
                type: 'button'
              },
            ]
          });
        })
        this.itemService.getAll().subscribe((resp) => {
          resp.content.forEach((e: IItem) => {
            if (res.content.findIndex((c: IPurchaseItem) => c.asin === e.asin) !== -1) {
              this.listaArticoli.push({ label: e.asin + " - " + e.title, value: e.asin })
            }
          });
          this.campiAcquisto.find(c => c.name === 'asin')!.value = resp.content[0].asin;
        })
      })
    })
  }

  editItem(event: any) {
    this.purchaseService.updateItem(event.purchase).subscribe((res) => {
      event.item.forEach((e: { nuovo: boolean; asin: string; quantityPurchase: number; unitPurchasePrice: number; modifica: boolean; elimina: boolean }) => {
        if (e.nuovo)
          this.purchaseItemService.insertItem({
            asin: e.asin,
            purchaseID: event.purchase.purchaseId,
            quantityPurchased: e.quantityPurchase,
            unitPurchasePrice: e.unitPurchasePrice
          }).subscribe((res) => { if (res) this.changeTab(res, "Aggiornato"); })
        else if (!e.nuovo && e.modifica && !e.elimina)
          this.purchaseItemService.updateItem({
            asin: e.asin,
            purchaseID: event.purchase.purchaseId,
            quantityPurchased: e.quantityPurchase,
            unitPurchasePrice: e.unitPurchasePrice
          }).subscribe((res) => { if (res) this.changeTab(res, "Aggiornato"); })
        else if (!e.nuovo && e.elimina)
          this.purchaseItemService.deleteItem(e.asin, event.purchase.purchaseId).subscribe((res) => { if (res) this.changeTab(res, "Aggiornato"); })
      });
    })
  }

  changeTab(res: boolean, title: string) {
    if (res) {
      this.caricoDati();
      Swal.fire(title + '!', 'L\'acquisto è stato ' + title.toLocaleLowerCase() + ' con successo.', 'success').then(() => {
        this.mostraBody = false;
        setTimeout(() => {
          this.mostraBody = true;
        }, 100);
      })
    }
    else {
      Swal.fire('Errore!', 'L\'acquisto non è stato ' + title.toLocaleLowerCase() + '.', 'error')
    }
  }

  deleteRow(event: ITableRow) {
    this.purchaseService.deleteItem(parseInt(event.cells[0].data, 10)).subscribe((res) => {
      if (res) {
        this.rows.splice(this.rows.findIndex(i => i.cells[0].data === event.cells[0].data), 1)
        Swal.fire('Eliminato!', 'L\'acquisto è stato eliminato.', 'success');
      }
      else {
        Swal.fire('Errore!', 'L\'acquisto non è stato eliminato.', 'error')
      }
    })
  }

  nuovoAcquisto(event: any) {
    this.purchaseService.insertItem(event.purchase).subscribe((res) => {
      if (res) {
        this.purchaseService.getAll(0, "purchaseId,desc", 1).subscribe((res) => {
          event.item.forEach((e: { asin: string; quantityPurchase: number; unitPurchasePrice: number; }) => {
            this.purchaseItemService.insertItem({
              asin: e.asin,
              purchaseID: res.content[0].purchaseId,
              quantityPurchased: e.quantityPurchase,
              unitPurchasePrice: e.unitPurchasePrice
            }).subscribe((res) => {
              if (res)
                this.changeTab(res, "Inserito");
            })
          });
        })
      }
    })
  }

  caricoMagazzino(event: ITableRow) {
    this.purchaseItemService.warehouseLoading(parseInt(event.cells[0].data, 10)).subscribe((res) => {
      if (res) {
        Swal.fire('Caricato', 'Il carico del magazzino è stato effettuato con successo.', 'success');
        this.caricoDati();
      }
    })
  }

  changePage(event: number) {
    this.caricoDati(event);
  }

  clearFilter(columnName: string) {
    this.caricoDati(0, columnName + ',asc');
  }

  downFilter() {
    this.caricoDati();
  }

  upFilter(columnName: string) {
    this.caricoDati(0, columnName + ',desc');
  }
}
