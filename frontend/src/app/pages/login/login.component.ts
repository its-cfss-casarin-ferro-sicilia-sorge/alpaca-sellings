import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import FormStatus from 'src/app/interfaces/FormStatus';
import { UserService } from 'src/app/services/user.service';
import * as Cookies from 'js-cookie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public loginForm: FormGroup;
  public loginFormStatus: FormStatus = { loading: false };

  public constructor(private userService: UserService, private readonly router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    this.loginForm.markAllAsTouched();

    if (this.loginForm.valid) {
      this.loginFormStatus = { loading: true };

      this.userService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(resp => {
        this.loginFormStatus = { loading: false, success: 'true' };

        // login con successo
        if (resp.valid) {
          Cookies.set('user', { token: resp.jwttoken, username: resp.username });
          this.userService.username = resp.username;
          this.router.navigate(['home']);
        } else {
          // errore
          this.loginFormStatus = { loading: false, error: 'Invalid credentials' };
        }
      }, () => {
        // errore
        this.loginFormStatus = { loading: false, error: 'Backend error' };
      });
    }
  }

}
