export default interface ITableColumn {
    title: string;
    name: string;
    type: string;
    sortType?: string;
    sortable?: boolean;
    searchable?: boolean;
    searchableValue?: any;
    classes?: string;
    styles?: string;
}
