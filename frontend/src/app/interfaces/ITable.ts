export default interface ITable {
    header?: string;
    classes?: string;
    styles?: string;
    noRowsText?: string;
}
