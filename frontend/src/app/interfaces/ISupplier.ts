export default interface ISupplier{
    supplierId: number;
    name: string;
    partitaIva: string;
    indirizzo: string;
    cellulare: string;
    email: string;
}
