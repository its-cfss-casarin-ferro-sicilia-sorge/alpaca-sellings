export default interface IItem{
    asin: string;
    title: string;
    brand: string;
    stock: number;
    price: number;
    category: string;
    lowStockLevel: number;
}
