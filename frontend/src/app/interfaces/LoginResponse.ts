export default interface LoginResponse {
    valid: boolean;
    jwttoken: string;
    username: string;
}
