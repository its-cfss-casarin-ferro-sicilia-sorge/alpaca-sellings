export default interface ITableCell {
    data: string;
    type?: 'text' | 'image' | 'button';
    isHtml?: boolean;
    classes?: string;
    styles?: string;
    routerLink?: string | string[];
    href?: string;
    src?: string;
}
