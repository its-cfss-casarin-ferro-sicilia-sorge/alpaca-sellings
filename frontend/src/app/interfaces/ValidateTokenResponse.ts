import IUser from "./IUser";

export default interface ValidateTokenResponse {
    valid: boolean;
    user: IUser;
}
