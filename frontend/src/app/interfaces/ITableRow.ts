import ITableCell from "./ITableCell";

export default interface ITableRow {
    cells: ITableCell[];
    routerLink?: string | string[];
    href?: string;
}
