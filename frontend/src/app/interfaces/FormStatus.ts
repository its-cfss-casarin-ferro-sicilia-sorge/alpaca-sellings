export default interface FormStatus {
    loading?: boolean;
    success?: string;
    error?: string;
}
