export default interface IFormCU {
    name: string;
    label: string;
    type: string;
    value?: any;
    disabled?: boolean;
    readonly?: boolean;
    selectValue?: {value: any, label: string}[];
    step?: string;
    min?: string;
    max?: string;
    maxlenght?: string;
    minlenght?: string;
}
