export default interface IPageNav {
    name: string;
    link?: string;
    active?: boolean;
}
