export default interface IOrdeItem{
    OrderItemID: string;
    AmazonOrderID: string;
    ASIN: string;
    Title: string;
    QuantityOrdered: number;
    QuantityShipped: number;
    PointsGrantedPointsNumber: string;
    PointsGrantedPointsMonetaryValueCurrencyCode: number;
    PointsGrantedPointsMonetaryValueAmount: string;
    ItemPriceCurrencyCode: number;
    ShippingPriceCurrencyCode: string;
    ShippingPriceAmount: number;
    PromotionIDs: string;
}
