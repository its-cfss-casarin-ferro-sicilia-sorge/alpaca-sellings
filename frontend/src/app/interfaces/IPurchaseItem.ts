export default interface IPurchaseItem{
    asin: string;
    purchaseID: number;
    quantityPurchased: number;
    unitPurchasePrice: number;
}
