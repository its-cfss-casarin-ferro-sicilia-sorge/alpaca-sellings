export default interface IPurchase{
    purchaseId: number;
    invoiceNumber: number;
    invoiceDate: Date;
    loadCarriedOut: boolean;
    supplierId: number;
}
