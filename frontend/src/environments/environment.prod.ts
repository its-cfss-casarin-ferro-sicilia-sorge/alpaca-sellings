export const environment = {
  production: true,
  backendUrl: 'https://alpacabackend.azurewebsites.net',
  services: {
    user: '/api/user',
    item: '/api/item',
    supplier: '/api/supplier',
    purchase: '/api/purchase',
    order: '/api/order',
    orderitem: '/api/orderItem',
    purchaseitem: '/api/purchaseItem',
    statistics: '/api/statistics'
  }
};
