package com.corvallis.alpaca.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corvallis.alpaca.repository.StatisticsRepository;

@RestController
@RequestMapping(value = "api/statistics")
public class StatisticsController {
	
	@Autowired
	private StatisticsRepository statisticsRepository;

	@GetMapping(produces = "application/json", value = "orders/{start}/{end}")
	public List<Object> orders(@PathVariable("start") @DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @PathVariable("end") @DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return statisticsRepository.orders(start, end);
	}
	
	@GetMapping(produces = "application/json", value = "orders")
	public List<Object> ordersAll() {
		return statisticsRepository.ordersAll();
	}

	@GetMapping(produces = "application/json", value = "asins/{start}/{end}")
	public List<Object> asins(@PathVariable("start") @DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @PathVariable("end") @DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return statisticsRepository.asins(start, end);
	}
	
	@GetMapping(produces = "application/json", value = "asins")
	public List<Object> asinsAll() {
		return statisticsRepository.asinsAll();
	}

	@GetMapping(produces = "application/json", value = "categories/{start}/{end}")
	public List<Object> categories(@PathVariable("start") @DateTimeFormat(pattern = "yyyy-MM-dd") Date start, @PathVariable("end") @DateTimeFormat(pattern = "yyyy-MM-dd") Date end) {
		return statisticsRepository.categories(start, end);
	}
	
	@GetMapping(produces = "application/json", value = "categories")
	public List<Object> categoriesAll() {
		return statisticsRepository.categoriesAll();
	}

}
