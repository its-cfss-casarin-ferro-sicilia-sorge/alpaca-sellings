package com.corvallis.alpaca.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.Supplier;
import com.corvallis.alpaca.dto.SupplierDto;
import com.corvallis.alpaca.repository.SupplierRepository;
import com.corvallis.alpaca.service.SupplierService;

@RestController
@RequestMapping(value = "api/supplier")
public class SupplierController {

	@Autowired
	private SupplierService supplierService;
	
	@Autowired
	private SupplierRepository supplierRepository;

	@GetMapping(produces = "application/json")
	public Page<Supplier> findAll(Pageable pageable) {
		return supplierRepository.findAll(pageable);
	}

	@GetMapping(value = "{id}", produces = "application/json")
	public Optional<SupplierDto> findById(@PathVariable("id") Integer id) {
		return supplierService.findById(id);
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() SupplierDto dto) {
		supplierService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() SupplierDto dto) {
		return supplierService.update(dto);
	}

	@DeleteMapping(value = "{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("id") Integer id) {
		return supplierService.deleteById(id);
	}

}