package com.corvallis.alpaca.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.OrderItem;
import com.corvallis.alpaca.dto.OrderItemDto;
import com.corvallis.alpaca.repository.OrderItemRepository;
import com.corvallis.alpaca.service.OrderItemService;

@RestController
@RequestMapping(value = "api/orderItem")
public class OrderItemController {

	@Autowired
	private OrderItemService orderItemService;
	
	@Autowired
	private OrderItemRepository orderItemRepository;

	@GetMapping(produces = "application/json")
	public Page<OrderItem> findAll(Pageable pageable) {
		return orderItemRepository.findAll(pageable);
	}

	@GetMapping(value = "{id}", produces = "application/json")
	public Optional<OrderItemDto> findById(@PathVariable("id") String id) {
		return orderItemService.findById(id);
	}
	
	@GetMapping(value = "/amazonOrderId/{amazonOrderId}", produces = "application/json")
	public Page<OrderItemDto> findByAmazonOrderID(@PathVariable("amazonOrderId") String id, Pageable pageable){
		return orderItemService.findByAmazonOrderId(id, pageable);
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() OrderItemDto dto) {
		orderItemService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() OrderItemDto dto) {
		return orderItemService.update(dto);
	}

	@DeleteMapping(value = "{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("id") String id) {
		return orderItemService.deleteById(id);
	}

}
