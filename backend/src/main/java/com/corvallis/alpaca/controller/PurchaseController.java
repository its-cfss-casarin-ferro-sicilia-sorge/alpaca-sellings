package com.corvallis.alpaca.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.Purchase;
import com.corvallis.alpaca.dto.PurchaseDto;
import com.corvallis.alpaca.repository.PurchaseRepository;
import com.corvallis.alpaca.service.PurchaseService;

@RestController
@RequestMapping(value = "api/purchase")
public class PurchaseController {

	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private PurchaseRepository purchaseRepository;

	@GetMapping(produces = "application/json")
	public Page<Purchase> findAll(Pageable pageable) {
		return purchaseRepository.findAll(pageable);
	}

	@GetMapping(value = "{id}", produces = "application/json")
	public Optional<PurchaseDto> findById(@PathVariable("id") Integer id) {
		return purchaseService.findById(id);
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() PurchaseDto dto) {
		purchaseService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() PurchaseDto dto) {
		return purchaseService.update(dto);
	}

	@DeleteMapping(value = "{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("id") Integer id) {
		return purchaseService.deleteById(id);
	}

}