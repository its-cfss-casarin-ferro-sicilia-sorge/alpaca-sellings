package com.corvallis.alpaca.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.Item;
import com.corvallis.alpaca.dto.ItemDto;
import com.corvallis.alpaca.repository.ItemRepository;
import com.corvallis.alpaca.service.ItemService;

@RestController
@RequestMapping(value = "api/item")
class ItemController {

	@Autowired
	private ItemService itemService;
	
	@Autowired 
	private ItemRepository itemRepository;
	
	//url per pagina 1, 2 elementi per pagina, ordinati per titolo: ../listPage?page=0&size=2&sort=title
	@GetMapping(produces = "application/json")
	public Page<Item> findAll(Pageable pageable) {
		return itemRepository.findAll(pageable);
	}

	@GetMapping(value = "{id}", produces = "application/json")
	public Optional<ItemDto> findById(@PathVariable("id") String id) {
		return itemService.findById(id);
	}
	
	@GetMapping(value = "prezzo/{asin}", produces = "application/json")
	public Double findPriceByAsin(@PathVariable("asin") String asin) {
		return itemService.findPriceByAsin(asin);
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() ItemDto dto) {
		itemService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() ItemDto dto) {
		return itemService.update(dto);
	}

	@DeleteMapping(value = "{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("id") String id) {
		return itemService.deleteById(id);
	}

}