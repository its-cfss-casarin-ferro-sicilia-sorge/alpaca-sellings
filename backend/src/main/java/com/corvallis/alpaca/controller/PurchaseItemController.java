package com.corvallis.alpaca.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.MyPurchaseItemKey;
import com.corvallis.alpaca.dao.PurchaseItem;
import com.corvallis.alpaca.dto.PurchaseItemDto;
import com.corvallis.alpaca.repository.PurchaseItemRepository;
import com.corvallis.alpaca.service.PurchaseItemService;

@RestController
@RequestMapping(value = "api/purchaseItem")
public class PurchaseItemController {

	@Autowired
	private PurchaseItemService purchaseItemService;

	@Autowired
	private PurchaseItemRepository purchaseItemRepository;

	@GetMapping(produces = "application/json")
	public Page<PurchaseItem> findAll(Pageable pageable) {
		return purchaseItemRepository.findAll(pageable);
	}

	@GetMapping(value = "purchaseid/{purchaseID}", produces = "application/json")
	public Page<PurchaseItem> findListByPurchaseId(@PathVariable("purchaseID") Integer purchaseID, Pageable pageable) {
		return purchaseItemRepository.findByPurchaseID(purchaseID, pageable);
	}

	@GetMapping(value = "{asin}/{id}", produces = "application/json")
	public Optional<PurchaseItemDto> findById(@PathVariable("asin") String asin,
			@PathVariable("id") Integer purchaseId) {
		return purchaseItemService.findById(asin, purchaseId);
	}

	@GetMapping(value = "warehouseLoading/{purchaseID}", produces = "application/json")
	public boolean wharehouseLoading(@PathVariable("purchaseID") Integer purchaseID) {
		try {
			purchaseItemService.wharehouseLoading(purchaseID);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() PurchaseItemDto dto) {
		purchaseItemService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() PurchaseItemDto dto) {
		return purchaseItemService.update(dto);
	}

	@DeleteMapping(value = "{asin}/{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("asin") String asin, @PathVariable("id") Integer purchaseId) {
		return purchaseItemService.deleteById(asin, purchaseId);
	}

}