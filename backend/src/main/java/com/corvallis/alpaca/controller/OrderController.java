package com.corvallis.alpaca.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.corvallis.alpaca.dao.Order;
import com.corvallis.alpaca.dto.OrderDto;
import com.corvallis.alpaca.repository.OrderRepository;
import com.corvallis.alpaca.service.OrderService;

@RestController
@RequestMapping(value = "api/order")
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@Autowired 
	private OrderRepository orderRepository;

	@GetMapping(produces = "application/json")
	public Page<Order> findAll(Pageable pageable) {
		return orderRepository.findAll(pageable);
	}

	@GetMapping(value = "{id}", produces = "application/json")
	public Optional<OrderDto> findById(@PathVariable("id") String id) {
		return orderService.findById(id);
	}

	@PostMapping(produces = "application/json")
	public boolean insert(@RequestBody() OrderDto dto) {
		orderService.insert(dto);
		return true;
	}

	@PutMapping(produces = "application/json")
	public boolean update(@RequestBody() OrderDto dto) {
		return orderService.update(dto);
	}

	@DeleteMapping(value = "{id}", produces = "application/json")
	public boolean deleteById(@PathVariable("id") String id) {
		return orderService.deleteById(id);
	}

}