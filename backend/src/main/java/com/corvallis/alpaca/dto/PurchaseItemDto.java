package com.corvallis.alpaca.dto;



public class PurchaseItemDto {
	private String asin;

	private Integer purchaseID;

	private Integer quantityPurchased;

	private Double unitPurchasePrice;

	public String getAsin() {
		return this.asin;
	}
	public PurchaseItemDto setAsin(String asin) {
		this.asin = asin;
		return this;
	}

	public Integer getPurchaseID() {
		return this.purchaseID;
	}
	public PurchaseItemDto setPurchaseID(Integer purchaseID) {
		this.purchaseID = purchaseID;
		return this;
	}

	public Integer getQuantityPurchased() {
		return this.quantityPurchased;
	}
	public PurchaseItemDto setQuantityPurchased(Integer quantityPurchased) {
		this.quantityPurchased = quantityPurchased;
		return this;
	}

	public Double getUnitPurchasePrice() {
		return this.unitPurchasePrice;
	}
	public PurchaseItemDto setUnitPurchasePrice(Double unitPurchasePrice) {
		this.unitPurchasePrice = unitPurchasePrice;
		return this;
	}

}