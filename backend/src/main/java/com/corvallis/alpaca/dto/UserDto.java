package com.corvallis.alpaca.dto;

import javax.persistence.Column;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
	
	@Column(name = "userID")
    private int userID;
	
	@Column(name = "username")
    private String username;
	
	@Column(name = "password")
    private String password;
	

}
