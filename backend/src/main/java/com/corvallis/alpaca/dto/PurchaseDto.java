package com.corvallis.alpaca.dto;

import java.util.Date;


public class PurchaseDto {
	private Integer purchaseId;

	private Integer invoiceNumber;

	private Date invoiceDate;

	private Boolean loadCarriedOut;

	private Integer supplierId;

	public Integer getPurchaseId() {
		return this.purchaseId;
	}
	public PurchaseDto setPurchaseId(Integer purchaseID) {
		this.purchaseId = purchaseID;
		return this;
	}

	public Integer getInvoiceNumber() {
		return this.invoiceNumber;
	}
	public PurchaseDto setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
		return this;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}
	public PurchaseDto setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
		return this;
	}

	public Boolean getLoadCarriedOut() {
		return this.loadCarriedOut;
	}
	public PurchaseDto setLoadCarriedOut(Boolean loadCarriedOut) {
		this.loadCarriedOut = loadCarriedOut;
		return this;
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}
	public PurchaseDto setSupplierId(Integer supplierID) {
		this.supplierId = supplierID;
		return this;
	}

}