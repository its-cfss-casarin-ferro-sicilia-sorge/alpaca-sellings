package com.corvallis.alpaca.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderItemDto {
	
	@JsonProperty("OrderItemId")
	private String orderItemId;

	@JsonProperty("AmazonOrderId")
	private String amazonOrderId;

	@JsonProperty("ASIN")
	private String asin;

	@JsonProperty("Title")
	private String title;

	@JsonProperty("QuantityOrdered")
	private Integer quantityOrdered;

	@JsonProperty("QuantityShipped")
	private Integer quantityShipped;

	@JsonProperty("PointsGrantedPointsNumber")
	private String pointsGrantedPointsNumber;

	@JsonProperty("PointsGrantedPointsMonetaryValueCurrencyCode")
	private String pointsGrantedPointsMonetaryValueCurrencyCode;

	@JsonProperty("PointsGrantedPointsMonetaryValueAmount")
	private Double pointsGrantedPointsMonetaryValueAmount;

	@JsonProperty("ItemPriceCurrencyCode")
	private String itemPriceCurrencyCode;
	
	@JsonProperty("ItemPriceAmount")
	private Double itemPriceAmount;

	@JsonProperty("ShippingPriceCurrencyCode")
	private String shippingPriceCurrencyCode;

	@JsonProperty("ShippingPriceAmount")
	private Double shippingPriceAmount;

	@JsonProperty("PromotionIds")
	private String promotionIDs;

}