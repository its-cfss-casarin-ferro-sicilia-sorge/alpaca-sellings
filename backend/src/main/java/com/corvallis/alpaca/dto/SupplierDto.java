package com.corvallis.alpaca.dto;


public class SupplierDto {
	private Integer supplierId;

	private String name;

	private String partitaIva;

	private String indirizzo;

	private String cellulare;

	private String email;

	public Integer getSupplierId() {
		return this.supplierId;
	}
	public SupplierDto setSupplierId(Integer supplierID) {
		this.supplierId = supplierID;
		return this;
	}

	public String getName() {
		return this.name;
	}
	public SupplierDto setName(String name) {
		this.name = name;
		return this;
	}

	public String getPartitaIva() {
		return this.partitaIva;
	}
	public SupplierDto setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
		return this;
	}

	public String getIndirizzo() {
		return this.indirizzo;
	}
	public SupplierDto setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
		return this;
	}

	public String getCellulare() {
		return this.cellulare;
	}
	public SupplierDto setCellulare(String cellulare) {
		this.cellulare = cellulare;
		return this;
	}

	public String getEmail() {
		return this.email;
	}
	public SupplierDto setEmail(String email) {
		this.email = email;
		return this;
	}

}