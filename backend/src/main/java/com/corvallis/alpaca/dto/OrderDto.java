package com.corvallis.alpaca.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderDto {

	@JsonProperty("AmazonOrderId")
	private String amazonOrderId;

	@JsonProperty("PurchaseDate")
	@JsonFormat(pattern = "yyyy-MM-dd' 'HH:mm:ss.SS")
	private Date purchaseDate;

	@JsonProperty("LastUpdateDate")
	@JsonFormat(pattern = "yyyy-MM-dd' 'HH:mm:ss.SS")
	private Date lastUpdateDate;

	@JsonProperty("OrderStatus")
	private String orderStatus;

	@JsonProperty("FulfillmentChannel")
	private String fulfillmentChannel;

	@JsonProperty("NumberOfItemsShipped")
	private Integer numberOfItemsShipped;

	@JsonProperty("NumberOfItemsUnshipped")
	private Integer numberOfItemsUnshipped;

	@JsonProperty("PaymentMethod")
	private String paymentMethod;

	@JsonProperty("PaymentMethodDetails")
	private String paymentMethodDetails;

	@JsonProperty("MarketplaceId")
	private String MarketplaceId;

	@JsonProperty("ShipmentServiceLevelCategory")
	private String shipmentServiceLevelCategory;

	@JsonProperty("OrderType")
	private String orderType;

	@JsonProperty("EarliestShipDate")
	@JsonFormat(pattern = "yyyy-MM-dd' 'HH:mm:ss.SS")
	private Date earliestShipDate;

	@JsonProperty("LatestShipDate")
	@JsonFormat(pattern = "yyyy-MM-dd' 'HH:mm:ss.SS")
	private Date latestShipDate;

	@JsonProperty("IsBusinessOrder")
	private Boolean isBusinessOrder;

	@JsonProperty("IsPrime")
	private Boolean isPrime;

	@JsonProperty("IsGlobalExpressEnabled")
	private Boolean isGlobalExpressEnabled;

	@JsonProperty("IsPremiumOrder")
	private Boolean isPremiumOrder;

	@JsonProperty("IsSoldByAB")
	private Boolean isSoldByAB;

	@JsonProperty("CompanyLegalName")
	private String companyLegalName;

	@JsonProperty("BuyerEmail")
	private String buyerEmail;

	@JsonProperty("BuyerName")
	private String buyerName;

	@JsonProperty("PurchaseOrderNumber")
	private String purchaseOrderNumber;

	@JsonProperty("ShippingAddressName")
	private String shippingAddressName;

	@JsonProperty("ShippingAddressLine1")
	private String shippingAddressLine1;

	@JsonProperty("ShippingAddressCity")
	private String shippingAddressCity;

	@JsonProperty("ShippingCityStateOrRegion")
	private String shippingCityStateOrRegion;

	@JsonProperty("ShippingStateOrRegionPostalCode")
	private String shippingStateOrRegionPostalCode;

}