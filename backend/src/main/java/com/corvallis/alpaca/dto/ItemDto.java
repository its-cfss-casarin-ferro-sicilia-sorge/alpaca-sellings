package com.corvallis.alpaca.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ItemDto {
	private String asin;

	private String title;

	private String brand;

	private Integer stock;

	private Double price;

	private String category;

	private Integer lowStockLevel;

	public String getAsin() {
		return this.asin;
	}
	public ItemDto setAsin(String asin) {
		this.asin = asin;
		return this;
	}

	public String getTitle() {
		return this.title;
	}
	public ItemDto setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getBrand() {
		return this.brand;
	}
	public ItemDto setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public Integer getStock() {
		return this.stock;
	}
	public ItemDto setStock(Integer stock) {
		this.stock = stock;
		return this;
	}

	public Double getPrice() {
		return this.price;
	}
	public ItemDto setPrice(Double price) {
		this.price = price;
		return this;
	}

	public String getCategory() {
		return this.category;
	}
	public ItemDto setCategory(String category) {
		this.category = category;
		return this;
	}

	public Integer getLowStockLevel() {
		return this.lowStockLevel;
	}
	public ItemDto setLowStockLevel(Integer lowStockLevel) {
		this.lowStockLevel = lowStockLevel;
		return this;
	}

}