package com.corvallis.alpaca.service;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.corvallis.alpaca.dao.Supplier;
import com.corvallis.alpaca.dto.SupplierDto;
import com.corvallis.alpaca.repository.SupplierRepository;

@Service
@Transactional
public class SupplierService {

	@Autowired
	private SupplierRepository supplierRepository;

	private SupplierDto daoToDto(Supplier dao) {
		SupplierDto dto = new SupplierDto();
		dto.setSupplierId(dao.getSupplierId());
		dto.setName(dao.getName());
		dto.setPartitaIva(dao.getPartitaIva());
		dto.setIndirizzo(dao.getIndirizzo());
		dto.setCellulare(dao.getCellulare());
		dto.setEmail(dao.getEmail());
		return dto;
	}

	private Supplier dtoToDao(SupplierDto dto) {
		Supplier dao = new Supplier();
		dao.setSupplierId(dto.getSupplierId());
		dao.setName(dto.getName());
		dao.setPartitaIva(dto.getPartitaIva());
		dao.setIndirizzo(dto.getIndirizzo());
		dao.setCellulare(dto.getCellulare());
		dao.setEmail(dto.getEmail());
		return dao;
	}

	public List<SupplierDto> findAll() {
		List<SupplierDto> dtos = new ArrayList<>();
		for (Supplier dao : supplierRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<SupplierDto> findById(Integer id) {
		Optional<Supplier> dao = supplierRepository.findById(id); 
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}

	public void insert(SupplierDto dto) {
		supplierRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public boolean update(SupplierDto dto) {
		if (supplierRepository.findById(dto.getSupplierId()).isPresent()) { 
			supplierRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(Integer id) {
		if (supplierRepository.findById(id).isPresent()) { 
			supplierRepository.deleteById(id);
			return true;
		}
		return false;
	}

}