package com.corvallis.alpaca.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.corvallis.alpaca.dao.OrderItem;
import com.corvallis.alpaca.dto.OrderItemDto;
import com.corvallis.alpaca.repository.OrderItemRepository;

@Service
@Transactional
public class OrderItemService {

	@Autowired
	private OrderItemRepository orderItemRepository;

	private OrderItemDto daoToDto(OrderItem dao) {
		OrderItemDto dto = new OrderItemDto();
		dto.setOrderItemId(dao.getOrderItemId());
		dto.setAmazonOrderId(dao.getAmazonOrderId());
		dto.setAsin(dao.getAsin());
		dto.setTitle(dao.getTitle());
		dto.setQuantityOrdered(dao.getQuantityOrdered());
		dto.setQuantityShipped(dao.getQuantityShipped());
		dto.setPointsGrantedPointsNumber(dao.getPointsGrantedPointsNumber());
		dto.setPointsGrantedPointsMonetaryValueCurrencyCode(dao.getPointsGrantedPointsMonetaryValueCurrencyCode());
		dto.setPointsGrantedPointsMonetaryValueAmount(dao.getPointsGrantedPointsMonetaryValueAmount());
		dto.setItemPriceCurrencyCode(dao.getItemPriceCurrencyCode());
		dto.setItemPriceAmount(dao.getItemPriceAmount());
		dto.setShippingPriceCurrencyCode(dao.getShippingPriceCurrencyCode());
		dto.setShippingPriceAmount(dao.getShippingPriceAmount());
		dto.setPromotionIDs(dao.getPromotionIDs());
		return dto;
	}

	private OrderItem dtoToDao(OrderItemDto dto) {
		OrderItem dao = new OrderItem();
		dao.setOrderItemId(dto.getOrderItemId());
		dao.setAmazonOrderId(dto.getAmazonOrderId());
		dao.setAsin(dto.getAsin());
		dao.setTitle(dto.getTitle());
		dao.setQuantityOrdered(dto.getQuantityOrdered());
		dao.setQuantityShipped(dto.getQuantityShipped());
		dao.setPointsGrantedPointsNumber(dto.getPointsGrantedPointsNumber());
		dao.setPointsGrantedPointsMonetaryValueCurrencyCode(dto.getPointsGrantedPointsMonetaryValueCurrencyCode());
		dao.setPointsGrantedPointsMonetaryValueAmount(dto.getPointsGrantedPointsMonetaryValueAmount());
		dao.setItemPriceCurrencyCode(dto.getItemPriceCurrencyCode());
		dao.setItemPriceAmount(dto.getItemPriceAmount());
		dao.setShippingPriceCurrencyCode(dto.getShippingPriceCurrencyCode());
		dao.setShippingPriceAmount(dto.getShippingPriceAmount());
		dao.setPromotionIDs(dto.getPromotionIDs());
		return dao;
	}

	public List<OrderItemDto> findAll() {
		List<OrderItemDto> dtos = new ArrayList<>();
		for (OrderItem dao : orderItemRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<OrderItemDto> findById(String id) {
		Optional<OrderItem> dao = orderItemRepository.findById(id); 
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}
	
	public Page<OrderItemDto> findByAmazonOrderId(String id, Pageable pageable) {
		Page<OrderItemDto> dtos = orderItemRepository.findByAmazonOrderId(id, pageable).map(dao -> daoToDto(dao));
		return dtos;
	}

	public void insert(OrderItemDto dto) {
		orderItemRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public void insertAll(OrderItemDto[] dto) {
		List<OrderItem> daos = Arrays.asList(Stream.of(dto).map(d -> this.dtoToDao(d)).toArray(OrderItem[]::new));
		orderItemRepository.saveAllAndFlush(daos);
		for (OrderItem dao : daos) {
			orderItemRepository.updateStock(dao.getQuantityOrdered(), dao.getAsin());
		}
	}

	public boolean update(OrderItemDto dto) {
		if (orderItemRepository.findById(dto.getOrderItemId()).isPresent()) { 
			orderItemRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(String id) {
		if (orderItemRepository.findById(id).isPresent()) { 
			orderItemRepository.deleteById(id);
			return true;
		}
		return false;
	}

}
