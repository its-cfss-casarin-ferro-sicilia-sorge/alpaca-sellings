package com.corvallis.alpaca.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.corvallis.alpaca.dao.Order;
import com.corvallis.alpaca.dao.WSOrderItems;
import com.corvallis.alpaca.dao.WSOrders;
import com.corvallis.alpaca.dto.OrderDto;
import com.corvallis.alpaca.repository.OrderRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemService orderItemService;

	@Value("${amazon.endpoint}")
	private String amazonEndpoint;

	private Logger logger = LoggerFactory.getLogger(OrderService.class);

	private OrderDto daoToDto(Order dao) {
		OrderDto dto = new OrderDto();
		dto.setAmazonOrderId(dao.getAmazonOrderId());
		dto.setPurchaseDate(dao.getPurchaseDate());
		dto.setLastUpdateDate(dao.getLastUpdateDate());
		dto.setOrderStatus(dao.getOrderStatus());
		dto.setFulfillmentChannel(dao.getFulfillmentChannel());
		dto.setNumberOfItemsShipped(dao.getNumberOfItemsShipped());
		dto.setNumberOfItemsUnshipped(dao.getNumberOfItemsUnshipped());
		dto.setPaymentMethod(dao.getPaymentMethod());
		dto.setPaymentMethodDetails(dao.getPaymentMethodDetails());
		dto.setMarketplaceId(dao.getMarketplaceId());
		dto.setShipmentServiceLevelCategory(dao.getShipmentServiceLevelCategory());
		dto.setOrderType(dao.getOrderType());
		dto.setEarliestShipDate(dao.getEarliestShipDate());
		dto.setLatestShipDate(dao.getLatestShipDate());
		dto.setIsBusinessOrder(dao.getIsBusinessOrder());
		dto.setIsPrime(dao.getIsPrime());
		dto.setIsGlobalExpressEnabled(dao.getIsGlobalExpressEnabled());
		dto.setIsPremiumOrder(dao.getIsPremiumOrder());
		dto.setIsSoldByAB(dao.getIsSoldByAB());
		dto.setCompanyLegalName(dao.getCompanyLegalName());
		dto.setBuyerEmail(dao.getBuyerEmail());
		dto.setBuyerName(dao.getBuyerName());
		dto.setPurchaseOrderNumber(dao.getPurchaseOrderNumber());
		dto.setShippingAddressName(dao.getShippingAddressName());
		dto.setShippingAddressLine1(dao.getShippingAddressLine1());
		dto.setShippingAddressCity(dao.getShippingAddressCity());
		dto.setShippingCityStateOrRegion(dao.getShippingCityStateOrRegion());
		dto.setShippingStateOrRegionPostalCode(dao.getShippingStateOrRegionPostalCode());
		return dto;
	}

	private Order dtoToDao(OrderDto dto) {
		Order dao = new Order();
		dao.setAmazonOrderId(dto.getAmazonOrderId());
		dao.setPurchaseDate(dto.getPurchaseDate());
		dao.setLastUpdateDate(dto.getLastUpdateDate());
		dao.setOrderStatus(dto.getOrderStatus());
		dao.setFulfillmentChannel(dto.getFulfillmentChannel());
		dao.setNumberOfItemsShipped(dto.getNumberOfItemsShipped());
		dao.setNumberOfItemsUnshipped(dto.getNumberOfItemsUnshipped());
		dao.setPaymentMethod(dto.getPaymentMethod());
		dao.setPaymentMethodDetails(dto.getPaymentMethodDetails());
		dao.setMarketplaceId(dto.getMarketplaceId());
		dao.setShipmentServiceLevelCategory(dto.getShipmentServiceLevelCategory());
		dao.setOrderType(dto.getOrderType());
		dao.setEarliestShipDate(dto.getEarliestShipDate());
		dao.setLatestShipDate(dto.getLatestShipDate());
		dao.setIsBusinessOrder(dto.getIsBusinessOrder());
		dao.setIsPrime(dto.getIsPrime());
		dao.setIsGlobalExpressEnabled(dto.getIsGlobalExpressEnabled());
		dao.setIsPremiumOrder(dto.getIsPremiumOrder());
		dao.setIsSoldByAB(dto.getIsSoldByAB());
		dao.setCompanyLegalName(dto.getCompanyLegalName());
		dao.setBuyerEmail(dto.getBuyerEmail());
		dao.setBuyerName(dto.getBuyerName());
		dao.setPurchaseOrderNumber(dto.getPurchaseOrderNumber());
		dao.setShippingAddressName(dto.getShippingAddressName());
		dao.setShippingAddressLine1(dto.getShippingAddressLine1());
		dao.setShippingAddressCity(dto.getShippingAddressCity());
		dao.setShippingCityStateOrRegion(dto.getShippingCityStateOrRegion());
		dao.setShippingStateOrRegionPostalCode(dto.getShippingStateOrRegionPostalCode());
		return dao;
	}

	public List<OrderDto> findAll() {
		List<OrderDto> dtos = new ArrayList<>();
		for (Order dao : orderRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<OrderDto> findById(String id) {
		Optional<Order> dao = orderRepository.findById(id);
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}

	public boolean existsByAmazonOrderId(String id) {
		return orderRepository.existsByAmazonOrderId(id);
	}

	public void insert(OrderDto dto) {
		orderRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public boolean update(OrderDto dto) {
		if (orderRepository.findById(dto.getAmazonOrderId()).isPresent()) {
			orderRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(String id) {
		if (orderRepository.findById(id).isPresent()) {
			orderRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Scheduled(fixedDelay = 1000 * 60 * 60)
	public void retrieveFromWebService() throws Exception {
		logger.info("Checking Amazon Selling for new orders...");

		try {
			WSOrders wsOrders = new ObjectMapper().readValue(new RestTemplate().getForEntity(amazonEndpoint
					+ "/orders?refresh_token=Atzr|IwEBIPGGbogA4gJ86OciHsp16r6gXmV&CreatedAfter=2021-06-01T16:09:52.000&CreatedBefore=2021-07-31T16:09:52.000",
					String.class).getBody(), WSOrders.class);

			long newItems = 0;
			for (OrderDto order : wsOrders.getOrders()) {
				if (!this.existsByAmazonOrderId(order.getAmazonOrderId())) {
					this.insert(order);
					logger.info("Order with Amazon Order Id {} added", order.getAmazonOrderId());
					newItems++;

					logger.info("Retrieving order items for order {}...", order.getAmazonOrderId());
					
					WSOrderItems orderItemDto = new ObjectMapper().readValue(new RestTemplate().getForEntity(amazonEndpoint
							+ "/orderitems?refresh_token=Atzr|IwEBIPGGbogA4gJ86OciHsp16r6gXmV&AmazonOrderId=" + order.getAmazonOrderId(),
							String.class).getBody(), WSOrderItems.class);
					
					orderItemService.insertAll(orderItemDto.getOrderItems());
					logger.info("Done");
				}
			}

			if (newItems == 0) {
				logger.info("No new orders found");
			} else {
				logger.info("{} new orders found", newItems);
			}
		} catch (ResourceAccessException e) {
			logger.error("Can't reach {}, maybe host is down or behind a firewall?", amazonEndpoint);
		}
	}

}