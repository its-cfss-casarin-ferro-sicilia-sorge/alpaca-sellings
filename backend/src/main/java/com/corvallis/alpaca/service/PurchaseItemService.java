package com.corvallis.alpaca.service;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.corvallis.alpaca.dao.MyPurchaseItemKey;
import com.corvallis.alpaca.dao.PurchaseItem;
import com.corvallis.alpaca.dto.PurchaseItemDto;
import com.corvallis.alpaca.repository.PurchaseItemRepository;
import com.corvallis.alpaca.repository.PurchaseRepository;

@Service
@Transactional
public class PurchaseItemService {

	@Autowired
	private PurchaseItemRepository purchaseItemRepository;
	
	@Autowired
	private PurchaseRepository purchaseRepository;

	private PurchaseItemDto daoToDto(PurchaseItem dao) {
		PurchaseItemDto dto = new PurchaseItemDto();
		dto.setAsin(dao.getAsin());
		dto.setPurchaseID(dao.getPurchaseID());
		dto.setQuantityPurchased(dao.getQuantityPurchased());
		dto.setUnitPurchasePrice(dao.getUnitPurchasePrice());
		return dto;
	}

	private PurchaseItem dtoToDao(PurchaseItemDto dto) {
		PurchaseItem dao = new PurchaseItem();
		dao.setAsin(dto.getAsin());
		dao.setPurchaseID(dto.getPurchaseID());
		dao.setQuantityPurchased(dto.getQuantityPurchased());
		dao.setUnitPurchasePrice(dto.getUnitPurchasePrice());
		return dao;
	}

	public List<PurchaseItemDto> findAll() {
		List<PurchaseItemDto> dtos = new ArrayList<>();
		for (PurchaseItem dao : purchaseItemRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<PurchaseItemDto> findById(String asin, Integer purchaseId) {
		Optional<PurchaseItem> dao = purchaseItemRepository.findById(new MyPurchaseItemKey(asin, purchaseId));
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}

	public void insert(PurchaseItemDto dto) {
		purchaseItemRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public boolean update(PurchaseItemDto dto) {
		if (purchaseItemRepository.findById(new MyPurchaseItemKey(dto.getAsin(), dto.getPurchaseID())).isPresent()) {
			purchaseItemRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(String asin, Integer purchaseId) {
		MyPurchaseItemKey itemKey = new MyPurchaseItemKey(asin, purchaseId);
		if (purchaseItemRepository.findById(itemKey).isPresent()) {
			purchaseItemRepository.deleteById(itemKey);
			return true;
		}
		return false;
	}
	
	public void wharehouseLoading(Integer purchaseId) {
		List<PurchaseItem> daos = purchaseItemRepository.findByPurchaseID(purchaseId);
		for (PurchaseItem dao : daos) {
			purchaseItemRepository.updateStock(dao.getQuantityPurchased(), dao.getAsin());
		}
		purchaseRepository.updateLoadCarriedOut(purchaseId);
	}

}