package com.corvallis.alpaca.service;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.corvallis.alpaca.dao.Purchase;
import com.corvallis.alpaca.dto.PurchaseDto;
import com.corvallis.alpaca.repository.PurchaseRepository;

@Service
@Transactional
public class PurchaseService {

	@Autowired
	private PurchaseRepository purchaseRepository;

	private PurchaseDto daoToDto(Purchase dao) {
		PurchaseDto dto = new PurchaseDto();
		dto.setPurchaseId(dao.getPurchaseId());
		dto.setInvoiceNumber(dao.getInvoiceNumber());
		dto.setInvoiceDate(dao.getInvoiceDate());
		dto.setLoadCarriedOut(dao.getLoadCarriedOut());
		dto.setSupplierId(dao.getSupplierId());
		return dto;
	}

	private Purchase dtoToDao(PurchaseDto dto) {
		Purchase dao = new Purchase();
		dao.setPurchaseId(dto.getPurchaseId());
		dao.setInvoiceNumber(dto.getInvoiceNumber());
		dao.setInvoiceDate(dto.getInvoiceDate());
		dao.setLoadCarriedOut(dto.getLoadCarriedOut());
		dao.setSupplierId(dto.getSupplierId());
		return dao;
	}

	public List<PurchaseDto> findAll() {
		List<PurchaseDto> dtos = new ArrayList<>();
		for (Purchase dao : purchaseRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<PurchaseDto> findById(Integer id) {
		Optional<Purchase> dao = purchaseRepository.findById(id); 
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}

	public void insert(PurchaseDto dto) {
		purchaseRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public boolean update(PurchaseDto dto) {
		if (purchaseRepository.findById(dto.getPurchaseId()).isPresent()) { 
			purchaseRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(Integer id) {
		if (purchaseRepository.findById(id).isPresent()) { 
			purchaseRepository.deleteById(id);
			return true;
		}
		return false;
	}

}