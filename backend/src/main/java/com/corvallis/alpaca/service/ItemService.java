package com.corvallis.alpaca.service;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.corvallis.alpaca.dao.Item;
import com.corvallis.alpaca.dto.ItemDto;
import com.corvallis.alpaca.repository.ItemRepository;

@Service
@Transactional
public class ItemService {

	@Autowired
	private ItemRepository itemRepository;

	private ItemDto daoToDto(Item dao) {
		ItemDto dto = new ItemDto();
		dto.setAsin(dao.getAsin());
		dto.setTitle(dao.getTitle());
		dto.setBrand(dao.getBrand());
		dto.setStock(dao.getStock());
		dto.setPrice(dao.getPrice());
		dto.setCategory(dao.getCategory());
		dto.setLowStockLevel(dao.getLowStockLevel());
		return dto;
	}

	private Item dtoToDao(ItemDto dto) {
		Item dao = new Item();
		dao.setAsin(dto.getAsin());
		dao.setTitle(dto.getTitle());
		dao.setBrand(dto.getBrand());
		dao.setStock(dto.getStock());
		dao.setPrice(dto.getPrice());
		dao.setCategory(dto.getCategory());
		dao.setLowStockLevel(dto.getLowStockLevel());
		return dao;
	}

	public List<ItemDto> findAll() {
		List<ItemDto> dtos = new ArrayList<>();
		for (Item dao : itemRepository.findAll()) {
			dtos.add(this.daoToDto(dao));
		}
		return dtos;
	}

	public Optional<ItemDto> findById(String id) {
		Optional<Item> dao = itemRepository.findById(id); 
		if (dao.isPresent()) {
			return Optional.of(this.daoToDto(dao.get()));
		}
		return Optional.empty();
	}
	
	public Double findPriceByAsin(String asin) {
		Optional<Item> dao = itemRepository.findById(asin); 
		if (dao.isPresent()) {
			return dao.get().getPrice();
		}
		return dao.get().getPrice();
	}

	public boolean existsByAsin(String id) {
		return itemRepository.existsByAsin(id); 
	}

	public void insert(ItemDto dto) {
		itemRepository.saveAndFlush(this.dtoToDao(dto));
	}

	public boolean update(ItemDto dto) {
		if (itemRepository.findById(dto.getAsin()).isPresent()) { 
			itemRepository.save(this.dtoToDao(dto));
			return true;
		}
		return false;
	}

	public boolean deleteById(String id) {
		if (itemRepository.findById(id).isPresent()) { 
			itemRepository.deleteById(id);
			return true;
		}
		return false;
	}

}