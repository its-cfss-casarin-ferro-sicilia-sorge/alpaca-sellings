package com.corvallis.alpaca;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.corvallis.alpaca.dto.ItemDto;
import com.corvallis.alpaca.service.ItemService;

@Component
public class StdItems implements CommandLineRunner {
	
	private final List<ItemDto> items = new ArrayList<>();

	@Autowired
	private ItemService itemService;
	
	public StdItems() {
		items.add(new ItemDto("B07D9SB7XW", "Minecraft", "Microsoft", 100, 25d, "Videogame", 10));
		items.add(new ItemDto("B07VK4QKBP", "Set bilancere", "Yoroi", 12, 50d, "Fitness", 20));
		items.add(new ItemDto("B08KSS6CLT", "The alla pesca", "EstaTè", 500, 2d, "Bevanda", 50));
		items.add(new ItemDto("B08123PCJH", "Aria Fritta", "Republicans", 4000, 1d, "Inutili", 0));
		items.add(new ItemDto("B07K495TYN", "Navaris Luce notturna LED Unicorno", "Chicco", 14, 10d, "Giocattoli", 2));
	}
	
	@Override
	public void run(String... args) throws Exception {
		for (ItemDto item : items) {
			if (!itemService.existsByAsin(item.getAsin())) {
				itemService.insert(item);
			}
		}
	}

}
