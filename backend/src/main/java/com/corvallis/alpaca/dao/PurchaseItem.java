package com.corvallis.alpaca.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "purchaseitems")
@IdClass(MyPurchaseItemKey.class)
public class PurchaseItem {
	@Id
	@Column(name = "asin")
	@Basic(optional = false)
	private String asin;

	@Id
	@Column(name = "purchaseid")
	@Basic(optional = true)
	private Integer purchaseID;

	@Column(name = "quantitypurchased")
	@Basic(optional = true)
	private Integer quantityPurchased;

	@Column(name = "unitpurchaseprice")
	@Basic(optional = true)
	private Double unitPurchasePrice;

	public String getAsin() {
		return this.asin;
	}
	public PurchaseItem setAsin(String asin) {
		this.asin = asin;
		return this;
	}

	public Integer getPurchaseID() {
		return this.purchaseID;
	}
	public PurchaseItem setPurchaseID(Integer purchaseID) {
		this.purchaseID = purchaseID;
		return this;
	}

	public Integer getQuantityPurchased() {
		return this.quantityPurchased;
	}
	public PurchaseItem setQuantityPurchased(Integer quantityPurchased) {
		this.quantityPurchased = quantityPurchased;
		return this;
	}

	public Double getUnitPurchasePrice() {
		return this.unitPurchasePrice;
	}
	public PurchaseItem setUnitPurchasePrice(Double unitPurchasePrice) {
		this.unitPurchasePrice = unitPurchasePrice;
		return this;
	}

}