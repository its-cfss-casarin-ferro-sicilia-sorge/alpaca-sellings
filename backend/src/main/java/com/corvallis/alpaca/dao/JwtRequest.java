package com.corvallis.alpaca.dao;

import java.io.Serializable;

public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    private String username;

    private String password;
    
    private boolean isValid = false;
    

    public JwtRequest()

    {

    }

    public JwtRequest(String username, String password, boolean isValid) {

        this.setUsername(username);

        this.setPassword(password);
        
        this.setIsValid(isValid);

    }

    public String getUsername() {

        return this.username;

    }

    public void setUsername(String username) {

        this.username = username;

    }

    public String getPassword() {

        return this.password;

    }

    public void setPassword(String password) {

        this.password = password;

    }
    
    public boolean getIsValid() {

        return this.isValid;

    }

    public void setIsValid(boolean isValid) {

        this.isValid = isValid;

    }

}
