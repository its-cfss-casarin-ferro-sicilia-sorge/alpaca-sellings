package com.corvallis.alpaca.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import java.util.Date;

@Entity
@Table(name = "purchases")
public class Purchase {
	@Id
	@GeneratedValue
	@Column(name = "purchaseid")
	@Basic(optional = false)
	private Integer purchaseId;

	@Column(name = "invoicenumber")
	@Basic(optional = true)
	private Integer invoiceNumber;

	@Column(name = "invoicedate")
	@Basic(optional = true)
	private Date invoiceDate;

	@Column(name = "loadcarriedout")
	@Basic(optional = true)
	private Boolean loadCarriedOut;

	@Column(name = "supplierid")
	@Basic(optional = true)
	private Integer supplierId;

	public Integer getPurchaseId() {
		return this.purchaseId;
	}
	public Purchase setPurchaseId(Integer purchaseID) {
		this.purchaseId = purchaseID;
		return this;
	}

	public Integer getInvoiceNumber() {
		return this.invoiceNumber;
	}
	public Purchase setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
		return this;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}
	public Purchase setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
		return this;
	}

	public Boolean getLoadCarriedOut() {
		return this.loadCarriedOut;
	}
	public Purchase setLoadCarriedOut(Boolean loadCarriedOut) {
		this.loadCarriedOut = loadCarriedOut;
		return this;
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}
	public Purchase setSupplierId(Integer supplierID) {
		this.supplierId = supplierID;
		return this;
	}

}