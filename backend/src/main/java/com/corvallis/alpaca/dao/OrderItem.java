package com.corvallis.alpaca.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "orderitems")
@Data
public class OrderItem {
	@Id
	@Column(name = "orderitemid")
	@Basic(optional = false)
	private String orderItemId;

	@Column(name = "amazonorderid")
	@Basic(optional = true)
	private String amazonOrderId;

	@Column(name = "asin")
	@Basic(optional = true)
	private String asin;

	@Column(name = "title")
	@Basic(optional = true)
	private String title;

	@Column(name = "quantityordered")
	@Basic(optional = true)
	private Integer quantityOrdered;

	@Column(name = "quantityshipped")
	@Basic(optional = true)
	private Integer quantityShipped;

	@Column(name = "pointsgrantedpointsnumber")
	@Basic(optional = true)
	private String pointsGrantedPointsNumber;

	@Column(name = "pointsgrantedpointsmonetaryvaluecurrencycode")
	@Basic(optional = true)
	private String pointsGrantedPointsMonetaryValueCurrencyCode;

	@Column(name = "pointsgrantedpointsmonetaryvalueamount")
	@Basic(optional = true)
	private Double pointsGrantedPointsMonetaryValueAmount;

	@Column(name = "itempricecurrencycode")
	@Basic(optional = true)
	private String itemPriceCurrencyCode;
	
	@Column(name = "itempriceamount")
	@Basic(optional = true)
	private Double itemPriceAmount;

	@Column(name = "shippingpricecurrencycode")
	@Basic(optional = true)
	private String shippingPriceCurrencyCode;

	@Column(name = "shippingpriceamount")
	@Basic(optional = true)
	private Double shippingPriceAmount;

	@Column(name = "promotionids")
	@Basic(optional = true)
	private String promotionIDs;

}