package com.corvallis.alpaca.dao;

import com.corvallis.alpaca.dto.OrderItemDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class WSOrderItems {
	@JsonProperty("OrderItems")
	OrderItemDto[] orderItems;
}
