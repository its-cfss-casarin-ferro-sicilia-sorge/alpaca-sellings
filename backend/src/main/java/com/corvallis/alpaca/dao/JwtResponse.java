package com.corvallis.alpaca.dao;

import java.io.Serializable;

public class JwtResponse implements Serializable{
	
	private static final long serialVersionUID = -8091879091924046844L;

    private final String jwttoken;
    private final boolean isValid;
    private final String username;

    public JwtResponse(String jwttoken, boolean isValid, String username) {

        this.jwttoken = jwttoken;
        this.isValid = isValid;
        this.username = username;

    }

	public String getJwttoken() {
		return jwttoken;
	}

	public boolean isValid() {
		return isValid;
	}

	public String getUsername() {
		return username;
	}

	

    

}
