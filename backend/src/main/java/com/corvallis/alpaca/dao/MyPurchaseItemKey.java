package com.corvallis.alpaca.dao;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class MyPurchaseItemKey implements Serializable {
	private String asin;
	private Integer purchaseID;
}