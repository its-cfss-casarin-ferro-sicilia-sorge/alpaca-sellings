package com.corvallis.alpaca.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "suppliers")
public class Supplier {
	@Id
	@GeneratedValue
	@Column(name = "supplierid")
	@Basic(optional = false)
	private Integer supplierId;

	@Column(name = "name")
	@Basic(optional = true)
	private String name;
	
	@Column(name = "partitaiva")
	@Basic(optional = true)
	private String partitaIva;
	
	@Column(name = "indirizzo")
	@Basic(optional = true)
	private String indirizzo;
	
	@Column(name = "cellulare")
	@Basic(optional = true)
	private String cellulare;
	
	@Column(name = "email")
	@Basic(optional = true)
	private String email;

	public String getPartitaIva() {
		return this.partitaIva;
	}
	public Supplier setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
		return this;
	}
	public String getIndirizzo() {
		return this.indirizzo;
	}
	public Supplier setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
		return this;
	}
	public String getCellulare() {
		return this.cellulare;
	}
	public Supplier setCellulare(String cellulare) {
		this.cellulare = cellulare;
		return this;
	}
	public String getEmail() {
		return this.email;
	}
	public Supplier setEmail(String email) {
		this.email = email;
		return this;
	}
	public Integer getSupplierId() {
		return this.supplierId;
	}
	public Supplier setSupplierId(Integer supplierID) {
		this.supplierId = supplierID;
		return this;
	}

	public String getName() {
		return this.name;
	}
	public Supplier setName(String name) {
		this.name = name;
		return this;
	}

}