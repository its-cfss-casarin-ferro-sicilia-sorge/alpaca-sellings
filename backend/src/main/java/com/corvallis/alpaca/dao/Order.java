package com.corvallis.alpaca.dao;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "orders")
@Data
public class Order {
	@Id
	@Column(name = "amazonorderid")
	@Basic(optional = false)
	private String amazonOrderId;

	@Column(name = "purchasedate")
	@Basic(optional = true)
	private Date purchaseDate;

	@Column(name = "lastupdatedate")
	@Basic(optional = true)
	private Date lastUpdateDate;

	@Column(name = "orderstatus")
	@Basic(optional = true)
	private String orderStatus;

	@Column(name = "fulfillmentchannel")
	@Basic(optional = true)
	private String fulfillmentChannel;

	@Column(name = "numberofitemsshipped")
	@Basic(optional = true)
	private Integer numberOfItemsShipped;

	@Column(name = "numberofitemsunshipped")
	@Basic(optional = true)
	private Integer numberOfItemsUnshipped;

	@Column(name = "paymentmethod")
	@Basic(optional = true)
	private String paymentMethod;

	@Column(name = "paymentmethoddetails")
	@Basic(optional = true)
	private String paymentMethodDetails;

	@Column(name = "marketplaceid")
	@Basic(optional = true)
	private String MarketplaceId;

	@Column(name = "shipmentservicelevelcategory")
	@Basic(optional = true)
	private String shipmentServiceLevelCategory;

	@Column(name = "ordertype")
	@Basic(optional = true)
	private String orderType;

	@Column(name = "earliestshipdate")
	@Basic(optional = true)
	private Date earliestShipDate;

	@Column(name = "latestshipdate")
	@Basic(optional = true)
	private Date latestShipDate;

	@Column(name = "isbusinessorder")
	@Basic(optional = true)
	private Boolean isBusinessOrder ;

	@Column(name = "isprime")
	@Basic(optional = true)
	private Boolean isPrime;

	@Column(name = "isglobalexpressenabled")
	@Basic(optional = true)
	private Boolean isGlobalExpressEnabled ;

	@Column(name = "ispremiumorder")
	@Basic(optional = true)
	private Boolean isPremiumOrder ;

	@Column(name = "issoldbyab")
	@Basic(optional = true)
	private Boolean isSoldByAB;

	@Column(name = "companylegalname")
	@Basic(optional = true)
	private String companyLegalName ;

	@Column(name = "buyeremail")
	@Basic(optional = true)
	private String buyerEmail;

	@Column(name = "buyername")
	@Basic(optional = true)
	private String buyerName;

	@Column(name = "purchaseordernumber")
	@Basic(optional = true)
	private String purchaseOrderNumber ;

	@Column(name = "shippingaddressname")
	@Basic(optional = true)
	private String shippingAddressName ;

	@Column(name = "shippingaddressline1")
	@Basic(optional = true)
	private String shippingAddressLine1 ;

	@Column(name = "shippingaddresscity")
	@Basic(optional = true)
	private String shippingAddressCity ;

	@Column(name = "shippingcitystateorregion")
	@Basic(optional = true)
	private String shippingCityStateOrRegion ;

	@Column(name = "shippingstateorregionpostalcode")
	@Basic(optional = true)
	private String shippingStateOrRegionPostalCode ;

}