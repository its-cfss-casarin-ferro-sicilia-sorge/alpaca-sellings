package com.corvallis.alpaca.dao;

import java.util.List;

import com.corvallis.alpaca.dto.OrderDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class WSOrders {
	@JsonProperty("Orders")
	List<OrderDto> orders;
}
