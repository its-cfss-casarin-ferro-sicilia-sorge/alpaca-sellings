package com.corvallis.alpaca.dao;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item {
	@Id
	@Column(name = "asin")
	@Basic(optional = false)
	private String asin;

	@Column(name = "title")
	@Basic(optional = true)
	private String title;

	@Column(name = "brand")
	@Basic(optional = true)
	private String brand;

	@Column(name = "stock")
	@Basic(optional = true)
	private Integer stock;

	@Column(name = "price")
	@Basic(optional = true)
	private Double price;

	@Column(name = "category")
	@Basic(optional = true)
	private String category;

	@Column(name = "lowstocklevel")
	@Basic(optional = true)
	private Integer lowStockLevel;

	public String getAsin() {
		return this.asin;
	}
	public Item setAsin(String asin) {
		this.asin = asin;
		return this;
	}

	public String getTitle() {
		return this.title;
	}
	public Item setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getBrand() {
		return this.brand;
	}
	public Item setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public Integer getStock() {
		return this.stock;
	}
	public Item setStock(Integer stock) {
		this.stock = stock;
		return this;
	}

	public Double getPrice() {
		return this.price;
	}
	public Item setPrice(Double price) {
		this.price = price;
		return this;
	}

	public String getCategory() {
		return this.category;
	}
	public Item setCategory(String category) {
		this.category = category;
		return this;
	}

	public Integer getLowStockLevel() {
		return this.lowStockLevel;
	}
	public Item setLowStockLevel(Integer lowStockLevel) {
		this.lowStockLevel = lowStockLevel;
		return this;
	}

}