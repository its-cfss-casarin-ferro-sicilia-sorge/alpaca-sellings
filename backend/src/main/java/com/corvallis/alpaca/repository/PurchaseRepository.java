package com.corvallis.alpaca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.corvallis.alpaca.dao.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {

	@Transactional
	@Modifying
	@Query("update Purchase p set p.loadCarriedOut = true where p.purchaseId = ?1")
	void updateLoadCarriedOut(Integer purchaseId);
}