package com.corvallis.alpaca.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.corvallis.alpaca.dao.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	Boolean existsByUsername(String username);
	
	
    Optional<User> findByUsername(String username);
	

}
