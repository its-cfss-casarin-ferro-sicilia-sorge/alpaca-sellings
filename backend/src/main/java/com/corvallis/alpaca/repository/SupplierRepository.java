package com.corvallis.alpaca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.corvallis.alpaca.dao.Supplier;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Integer> { }