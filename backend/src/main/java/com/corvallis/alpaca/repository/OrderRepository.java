package com.corvallis.alpaca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.corvallis.alpaca.dao.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

	boolean existsByAmazonOrderId(String id);

}