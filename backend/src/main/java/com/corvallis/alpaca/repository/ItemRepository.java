package com.corvallis.alpaca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.corvallis.alpaca.dao.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, String> {

	boolean existsByAsin(String id);
	
}