package com.corvallis.alpaca.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.corvallis.alpaca.dao.MyPurchaseItemKey;
import com.corvallis.alpaca.dao.PurchaseItem;

@Repository
public interface PurchaseItemRepository extends JpaRepository<PurchaseItem, MyPurchaseItemKey> { 
	
	Page<PurchaseItem> findByPurchaseID(Integer purchaseID, Pageable pageable);
	List<PurchaseItem> findByPurchaseID(Integer purchaseID);
	
	@Transactional
	@Modifying
	@Query("update Item i set i.stock = (i.stock + ?1) where i.asin = ?2")
	void updateStock(Integer stock, String asin);
}