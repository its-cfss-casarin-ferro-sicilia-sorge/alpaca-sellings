package com.corvallis.alpaca.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.corvallis.alpaca.dao.Order;

@Repository
public interface StatisticsRepository extends JpaRepository<Order, Integer> {

	// select orders.amazonorderid, SUM(orderitems.itempriceamount * orderitems.quantityordered) as sum from orders join orderitems on orders.amazonorderid = orderitems.amazonorderid where purchasedate between '2021-06-21' and '2021-06-28' group by orders.amazonorderid;
	@Query("select o.amazonOrderId, SUM(oi.itemPriceAmount * oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId where o.purchaseDate between ?1 and ?2 group by o.amazonOrderId")
	List<Object> orders(Date start, Date end);
	
	@Query("select i.title, SUM(oi.itemPriceAmount * oi.quantityOrdered), SUM(oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId join Item i on oi.asin = i.asin where o.purchaseDate between ?1 and ?2 group by oi.asin, i.title")
	List<Object> asins(Date start, Date end);
	
	@Query("select i.category, SUM(oi.itemPriceAmount * oi.quantityOrdered), SUM(oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId join Item i on oi.asin = i.asin where o.purchaseDate between ?1 and ?2 group by i.category")
	List<Object> categories(Date start, Date end);
	
	@Query("select o.amazonOrderId, SUM(oi.itemPriceAmount * oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId group by o.amazonOrderId")
	List<Object> ordersAll();
	
	@Query("select i.title, SUM(oi.itemPriceAmount * oi.quantityOrdered), SUM(oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId join Item i on oi.asin = i.asin group by oi.asin, i.title")
	List<Object> asinsAll();
	
	@Query("select i.category, SUM(oi.itemPriceAmount * oi.quantityOrdered), SUM(oi.quantityOrdered) as sum from Order o join OrderItem oi on o.amazonOrderId = oi.amazonOrderId join Item i on oi.asin = i.asin group by i.category")
	List<Object> categoriesAll();
	
}
