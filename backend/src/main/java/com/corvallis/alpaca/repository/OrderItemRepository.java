package com.corvallis.alpaca.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.corvallis.alpaca.dao.OrderItem;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, String> {

	Page<OrderItem> findByAmazonOrderId(String id, Pageable pageable); 

	@Transactional
	@Modifying
	@Query("update Item i set i.stock = (i.stock - ?1) where i.asin = ?2")
	void updateStock(Integer stock, String asin);
}
