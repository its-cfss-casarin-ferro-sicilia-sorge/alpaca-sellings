FROM openjdk:11-jdk

WORKDIR /root/app

RUN mkdir /root/.m2

ENTRYPOINT [ "/bin/sh" ]